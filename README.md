# TagTerminal

TagTerminal is a library for applications which uses a command line interface (CLI) to manage a lot of data items.
The items can be distributed on different 'stores' and grouped by tags.

This is the base library for "Xtended Memory 2". Please take a look!

## License
GNU Lesser General Public License version 3

