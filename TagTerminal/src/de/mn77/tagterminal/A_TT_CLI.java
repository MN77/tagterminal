/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal;

import java.io.IOException;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.lib.terminal.CSI_COLOR_BG;
import de.mn77.lib.terminal.CSI_COLOR_FG;
import de.mn77.lib.terminal.MTerminal;
import de.mn77.tagterminal.control.I_TaskControl;
import de.mn77.tagterminal.control.TaskControl;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public abstract class A_TT_CLI {

	final I_TaskControl override;


	public A_TT_CLI() {
		this.override = null;
	}

	public A_TT_CLI( final I_TaskControl override ) {
		this.override = override;
	}


	public boolean runTerminal( final MTerminal terminal, final String in, final I_DataStoreManager dsm, final TaskControl tc ) throws IOException, Err_FileSys {
		String uin = in;

		if( uin == null )
			uin = this.iReadLine( terminal, tc );

		// If it's still null after read:
		if( uin == null || uin.equals( "exit" ) || uin.equals( "quit" ) || uin.equals( "q" ) ) { // || uin.equals("_APP.exit") || uin.equals("§exit")  // uin.equals("e")
// 			terminal.print("\n", CSI.cursorToRow(1));
			final String newLine = terminal.isInRawMode() ? "\r\n" : "\n";
			terminal.print( newLine );
			terminal.print( this.pExitMessage() + newLine ); // TAG_TERMINAL.INDENT_STRING +
			terminal.print( newLine );
			terminal.closeTerminal();
			return false;
		}

		final boolean rawBefore = terminal.temporaryCooked();
		this.exec( uin, dsm, tc );
		terminal.temporaryReset( rawBefore );

		// Terminal can be null, if argument is used
		// Color is only possible in raw mode!
//		if(terminal != null)
//			if(terminal.isInRawMode()) {
//				final COLOR_FG color = execOkay ? COLOR_FG.GREEN : COLOR_FG.RED;
//				output = Lib_String.replace(output, '\n', "\n\r");
//				terminal.print(color, output, "\r\n", COLOR_FG.DEFAULT);
//			}
//			else
//				terminal.print(output, "\n");

		return true;
	}

	public void start( final String config ) throws Err_FileSys, IOException {
		this.start( config, null);
	}

	public void start( String config, String[] args ) throws Err_FileSys, IOException {
//		Lib_Version.initKeep(Main_Xemy.VERSION);

		// start CLI
		final MTerminal terminal = new MTerminal();
		if( this.pIsDebug() )
			terminal.setDebug();

		if( terminal.isInRawMode() )
			MOut.setLineBreak( "\r\n" );

		Runtime.getRuntime().addShutdownHook( new Thread() {

			@Override
			public void run() {
				terminal.closeTerminal();
			}

		} );

		this.pHeadline( terminal );

		final I_DataStoreManager dsm = this.pConnect( config );

		final TaskControl tc = new TaskControl( this.override );

		String startCmd = this.pStartCmd();

		if(args != null) {
			startCmd = null;

			try {
				tc.exec(dsm, args);
			}
			catch( final RuntimeException re ) {
				if( this.pIsDebug() )
					Err.show( re );
				else
					MOut.error( re.getMessage() );
			}
		}

		// --- Start loop ---
		boolean run = true;

		while( run ) {
			run = this.runTerminal( terminal, startCmd, dsm, tc );
			startCmd = null;
		}
//		MOut.print("Good Bye!");
		dsm.close();
	}

	protected abstract I_DataStoreManager pConnect( String config );

	protected abstract String pExitMessage();

	protected abstract void pHeadline( MTerminal terminal );

	protected abstract boolean pIsDebug();

	protected abstract Object pPromptEnd();

	protected abstract Object pPromptStart();

	protected abstract String pStartCmd();

	private void exec( String s, final I_DataStoreManager dsm, final TaskControl tc ) throws Err_FileSys {
		s = s.trim();

//		String cmd = s.substring(0, i);
//		String argsString = s.substring(i);
		final String[] args = s.split( " " );

		try {
			tc.exec( dsm, args );
		}
		catch( final RuntimeException re ) {
			if( this.pIsDebug() )
				Err.show( re );
			else
				MOut.error( re.getMessage() );
		}
	}

	private String iPrompt( final IdMap map ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.pPromptStart() );
		sb.append( map.prompt( "- ", "? " ) );
//		sb.append("|");
		sb.append( this.pPromptEnd() );
		return sb.toString();
	}

	/**
	 * Result can be null
	 */
	private String iReadLine( final MTerminal terminal, final TaskControl tc ) throws IOException {
		final IdMap map = tc.getIDMap();
		final String prompt = this.iPrompt( map );

		if( terminal.isInRawMode() ) {
			terminal.print( CSI_COLOR_BG.DEFAULT, CSI_COLOR_FG.LIGHTBLUE, prompt, CSI_COLOR_FG.DEFAULT );
			final String input = terminal.readLine();
			terminal.print( "\r\n" );
			return input;
		}
		else {
			terminal.print( prompt );
			return terminal.readLine();
		}
	}

}
