/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.mn77.base.data.struct.SimpleMap;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.SysDir;
import de.mn77.miniconf.MIniConf;
import de.mn77.miniconf.result.MIniConfObject;
import de.mn77.miniconf.result.MIniConfResult;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_External;


/**
 * @author Michael Nitsche
 * @created 04.12.2021
 */
public abstract class A_TT_Config implements I_TT_Config {

	public static final String DIRNAME = "tagterminal";

	private MIniConfResult            conf       = null;
	private I_Store                   writeStore = null;
	private HashMap<Integer, I_Store> stores     = null;
	private final String              configFileName;


	public A_TT_Config( String configName ) {
		Err.ifNull( configName );
		configName = configName.trim().toLowerCase();

		if( configName.length() == 0 )
			throw Err.invalid( "No config name defined!" );

		this.configFileName = configName.endsWith( ".conf" )
			? configName
			: configName + ".conf";
	}


	@Override
	public String getEditEditor() {
		return this.conf.getString( "edit.editor", Lib_External.EDITORS[0] );
	}

	@Override
	public String getEditEditorX() {
		return this.conf.getString( "edit.xeditor", Lib_External.XEDITORS[0] );
	}


	@Override
	public boolean getExperimental() {
		return this.conf.getBoolean( "general.experimental", false );
	}


	@Override
	public String getShowFormat() {
		return this.conf.getString( "show.format", this.pDefaultShowFormat() ).toLowerCase();
	}

	@Override
	public String getShowViewer() {
		return this.conf.getString( "show.viewer", this.pDefaultShowViewer() );
	}

	@Override
	public Map<Integer, I_Store> getStoreMap() {
		if( this.iAllActive() )
			return this.stores;
		else {
			final SimpleMap<Integer,I_Store> result = new SimpleMap<>();
			for(Entry<Integer, I_Store> store : this.stores.entrySet())
				if(store.getValue().isEnabled())
					result.add( store.getKey() , store.getValue() );
			return result;
		}
	}

	@Override
	public Map<Integer, I_Store> getStoreMapFull() {
		return this.stores;
	}

	private boolean iAllActive() {
		int active = 0;

		for(I_Store store : this.stores.values())
			if(store.isEnabled())
				active++;

		return active == this.stores.size();
	}


//	@Override
//	public I_Store[] getStores() {
//		if( this.iAllActive() )
//			return this.stores.values().toArray( new I_Store[this.stores.size()] );
//		else {
//			final SimpleList<I_Store> result = new SimpleList<>(this.stores.size());
//			for(I_Store store : this.stores.values())
//				if(store.isEnabled())
//					result.add( store );
//			return result.toArray( new I_Store[result.size()] );
//		}
//	}

	@Override
	public I_Store getWriteStore( final boolean canBeNull ) {
		if( this.conf == null )
			throw new Err_Runtime( "Config not read yet." );

		if( this.writeStore == null ) {
			Integer key = null;
			for( final Integer i : this.stores.keySet() )
				if( key == null || i < key )
					key = i;

			if( key != null )
				return this.stores.get( key );
			else if( canBeNull )
				return null;
			else
				throw new TagTerminalException( "No store available to write!" );
		}

		return this.writeStore;
	}


	public I_Directory getConfigLinkDir() throws Err_FileSys {
		return SysDir.userConfig( A_TT_Config.DIRNAME );
	}

	@Override
	public void read() throws Err_FileSys {
		// Read config

		final I_Directory configDir = SysDir.userConfig( A_TT_Config.DIRNAME ).dirFlex( this.getConfigDirName() );
		final File configFile = configDir.fileMay( this.configFileName ).getFile();

		if( !configFile.exists() )
			// Create default config
			this.pWriteDefaultConfig( configDir, configFile );

		this.conf = MIniConf.parse( configFile );

		// Read stores
		this.stores = new HashMap<>();

		for( int i = 1; i <= 9; i++ ) { // 0 is bad, because of leading zero. And, 0 will be used internal
			final MIniConfObject ds = this.conf.getObject( I_TT_Config.CONFIG_FOLDER_DATASTORES + i, null );
			if( ds != null ) {
				final I_Store store = this.pObjectToStore( i, ds );

				if(store == null)
					throw new Err_Runtime("Unknown store type: " + ds.type);
				else
					this.stores.put( i, store );
			}
		}

		// Read default store
		final int writeKey = this.conf.getInteger( "general.store", 0 );
		this.writeStore = this.stores.getOrDefault( writeKey, null );

//		MOut.print("-----");
//		for(Map.Entry<String,String> e : this.conf.getItems()) {
//			MOut.print(e.getKey() + " --> "+e.getValue());
//		}
//		MOut.print("-----");
	}


	protected abstract String pDefaultShowFormat();

	protected abstract String pDefaultShowViewer();

	protected abstract I_Store pObjectToStore( int i, MIniConfObject ds );

	protected abstract void pWriteDefaultConfig( I_Directory configDir, File configFile );

}
