/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.config;

import java.util.Map;

import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.tagterminal.store.I_Store;


/**
 * @author Michael Nitsche
 * @created 23.01.2023
 */
public interface I_TT_Config {

	String DEFAULT_CONFIG_FILENAME  = "default.conf";
	String DEFAULT_STORE_FILENAME   = "store.sqlite3";
	String CONFIG_FOLDER_DATASTORES = "stores.";


	String getConfigDirName();

	String getEditEditor();

	String getEditEditorX();

	boolean getExperimental();

	String getShowFormat();

	String getShowViewer();

	Map<Integer, I_Store> getStoreMap();

	Map<Integer, I_Store> getStoreMapFull();

	I_Store getWriteStore( boolean canBeNull );

	void read() throws Err_FileSys;

	I_Directory getConfigLinkDir() throws Err_FileSys;

}
