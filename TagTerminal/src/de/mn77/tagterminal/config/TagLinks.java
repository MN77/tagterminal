/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.config;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.keypot.KeyPotItem;
import de.mn77.base.data.struct.keypot.KeyPotSet;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 12.09.2022
 */
public class TagLinks {

	private static final String CONFIG_FILE = "linked.csv";

	private final KeyPotSet<String, String> map = new KeyPotSet<>();
	private final File                      configFile;


	public TagLinks( final I_Directory configDir ) {
		this.configFile = configDir.fileMay( TagLinks.CONFIG_FILE ).getFile();
	}


	public I_Table<String> getAll() {
		I_Table<String> result = new ArrayTable<>(2);

		for(KeyPotItem<String, String> item : this.map)
			for( String val : item.pot)
				result.addRow( item.key, val );

		return result;
	}

	public void link( final String a, final String b ) throws Err_FileSys {
		if( a.equals( b ) )
			return;

		final boolean added = this.map.put( a, b );

		if( added ) {
			final String line = a + "," + b + "\n";

			try {
				Lib_TextFile.append( this.configFile, line );
			}
			catch( final Err_FileSys e ) {
				Err.show( e ); // TODO improve
			}
		}
	}

	public Collection<String> linkedWith( final String tag ) {
		final HashSet<String> result = new HashSet<>();

		final I_Collection<String> set = this.map.getPot( tag, null );

		if( set != null ) {
			result.addAll( set );

			for( final String key : set )
				if( !key.equals( tag ) ) { // Prevent cycle
					final Collection<String> sub = this.linkedWith( key );
					result.addAll( sub );
				}
		}

		return result;
	}

	public void read() throws Err_FileSys {
		if( !this.configFile.exists() )
			return;
		final String read = Lib_TextFile.read( this.configFile );
		final List<String> lines = ConvertString.toLines( read );
		for( final String line : lines )
			if( line.length() > 0 ) {
				final String[] key_value = line.split( "," );

				if( key_value.length != 2 ) {
					MOut.print( "Invalid line: " + line );
					continue;
				}

				if( key_value[1].length() == 0 )
					this.map.remove( key_value[0], key_value[1] );
				else
					this.map.put( key_value[0], key_value[1] );
			}
	}

	public boolean unlink( final String a, final String b ) throws Err_FileSys {
		final boolean removed = this.map.remove( a, b );
		if( removed )
			this.write();

		return removed;
	}

	public void write() throws Err_FileSys {
		final StringBuilder sb = new StringBuilder();

		for( final KeyPotItem<String, String> entry : this.map ) {
			final String key = entry.key;

			for( final String value : entry.pot ) {
				sb.append( key );
				sb.append( ',' );
				sb.append( value );
				sb.append( '\n' );
			}
		}

		Lib_TextFile.set( this.configFile, sb.toString() );
	}

}
