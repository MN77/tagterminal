/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.control;

import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_DataBase;
import de.mn77.base.sys.MOut;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.task.META_TYPE;
import de.mn77.tagterminal.task.Task_All;
import de.mn77.tagterminal.task.Task_Classify;
import de.mn77.tagterminal.task.Task_Clean;
import de.mn77.tagterminal.task.Task_Clone;
import de.mn77.tagterminal.task.Task_Combine;
import de.mn77.tagterminal.task.Task_CopyMove;
import de.mn77.tagterminal.task.Task_Delete;
import de.mn77.tagterminal.task.Task_Describe;
import de.mn77.tagterminal.task.Task_Diff;
import de.mn77.tagterminal.task.Task_Edit;
import de.mn77.tagterminal.task.Task_Export;
import de.mn77.tagterminal.task.Task_Help;
import de.mn77.tagterminal.task.Task_Import;
import de.mn77.tagterminal.task.Task_Keys;
import de.mn77.tagterminal.task.Task_Link;
import de.mn77.tagterminal.task.Task_ListLast;
import de.mn77.tagterminal.task.Task_ListLinks;
import de.mn77.tagterminal.task.Task_ListNewest;
import de.mn77.tagterminal.task.Task_ListSize;
import de.mn77.tagterminal.task.Task_ListStores;
import de.mn77.tagterminal.task.Task_ListTags;
import de.mn77.tagterminal.task.Task_ListTop;
import de.mn77.tagterminal.task.Task_ListTrash;
import de.mn77.tagterminal.task.Task_Mapping;
import de.mn77.tagterminal.task.Task_Merge;
import de.mn77.tagterminal.task.Task_MetaInfo;
import de.mn77.tagterminal.task.Task_New;
import de.mn77.tagterminal.task.Task_OnOff;
import de.mn77.tagterminal.task.Task_Rename;
import de.mn77.tagterminal.task.Task_Search;
import de.mn77.tagterminal.task.Task_SearchTags;
import de.mn77.tagterminal.task.Task_Show;
import de.mn77.tagterminal.task.Task_Size;
import de.mn77.tagterminal.task.Task_Tree;
import de.mn77.tagterminal.task.Task_UnDelete;
import de.mn77.tagterminal.task.Task_Unlink;
import de.mn77.tagterminal.task.Task_Unmap;
import de.mn77.tagterminal.task.Task_Version;
import de.mn77.tagterminal.util.Lib_ItemID;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 17.02.2022
 */
public class TaskControl {

	private final I_TaskControl override;
	private IdMap               idMap = new IdMap();


	public TaskControl() {
		this.override = null;
	}

	public TaskControl( final I_TaskControl tc ) {
		this.override = tc;
	}


	public void exec( final I_DataStoreManager dsm, final String[] args ) {

		try {
			this.idMap = this.runTask( dsm, args );
			Err.ifNull( this.idMap );
			this.idMap.setSingleLastID();
		}
		catch( final TagTerminalException se ) {
			Lib_TTOut.print( se.getMessage() );
		}
		catch(final Err_DataBase edb) {
			Err.show( edb );
			final String message = "Database connection error.\nApplication will be terminated! Sorry!";
			MOut.error( message );
			System.exit( 1 );
		}
		catch( final Exception e ) {
			Err.show( e );
		}
//		Lib_TTOut.print(); // Linebreak after every command
	}

	public IdMap getIDMap() {
		return this.idMap;
	}

	private String[] iSplitSingle( final I_DataStoreManager dsm, String[] sa ) {
//		if(dsm.getExperimental() && args.length == 1 && args[0].matches("[a-zA-Z][0-9]+"))
//			args = new String[] {""+args[0].charAt(0), args[0].substring(1)};
//		&& sa[0].matches("[a-zA-Z][0-9]+")

		if( sa.length >= 1 && sa[0].length() > 1 ) {
			final char c0 = sa[0].charAt( 0 );
			boolean hit = false;

//			if(c0 == '?')
//				hit = true;
//			else {
			final char c1 = sa[0].charAt( 1 );
			if( (c0 >= 'a' && c0 <= 'z' || c0 >= 'A' && c0 <= 'Z') && c1 >= '0' && c1 <= '9' )
				hit = true;
//			}

			if( hit ) {
				final String command = "" + c0;
				sa[0] = sa[0].substring( 1 );
				sa = Lib_Array.begin( String.class, sa, command );
			}
		}
		return sa;
	}

	private IdMap runTask( final I_DataStoreManager dsm, String[] args ) {

		// No Args
		if( Lib_Task.emptyInput( args ) ) {
			if( this.idMap.hasSelection() )
				return this.iTaskDefault(dsm, new String[]{ this.idMap.computeLastID() });
			if( this.idMap.hasContent() )
				return new Task_Keys( dsm, this.idMap, null ).exec();

			IdMap result = null;

			if( this.override != null )
				result = this.override.runEmpty( dsm, this.idMap );

			return result != null
				? result
				: new Task_Help( dsm, this.idMap, null, true ).exec();
		}

		// Default
		if( args[0].matches( "[0-9" + Lib_ItemID.ABSOLUTE_ID_DELIMITER + "]+" ) )
			if( args.length == 1 ) {
				args = new String[]{ null, args[0] }; // First argument will be ignored
				return this.iTaskDefault(dsm, args);
			}
			else
				args = Lib_Array.begin( String.class, args, "class" ); //classify

		// Split char+ID	- Experimentiell - Kann die Syntax verwaschen!
		args = this.iSplitSingle( dsm, args );

		if( this.override != null ) {
			final IdMap result = this.override.runTask( args[0], dsm, this.idMap, args );
			if( result != null )
				return result;
		}

		switch( args[0] ) {
			case "?":
			case "k":
			case "keys":
				return new Task_Keys( dsm, this.idMap, args ).exec();
			case "m":
			case "map":
//			case "mapping":
				return new Task_Mapping( dsm, this.idMap, args ).exec();

			case "a": // combine 'a' and 'all' with 's' and 'S'?
			case "all":
				return new Task_All( dsm, args ).exec();

			case "s":
//			case "sall":
			case "search":
				return new Task_Search( dsm, this.idMap, args, false, false ).exec();
			case "S":
//			case "sany":
			case "Search":
				return new Task_Search( dsm, this.idMap, args, true, false ).exec();

			case "f":
			case "find":
				return new Task_Search( dsm, this.idMap, args, false, true ).exec();
			case "F":
			case "Find":
				return new Task_Search( dsm, this.idMap, args, true, true ).exec();

			case "l":
			case "link":
				return new Task_Link( dsm, this.idMap, args ).exec();
			case "L":
			case "unlink":
				return new Task_Unlink( dsm, this.idMap, args ).exec();
			case "links":
				return new Task_ListLinks( dsm, this.idMap, args ).exec();

			case "top":
				return new Task_ListTop( dsm, this.idMap, args ).exec();
			case "newest":
				return new Task_ListNewest( dsm, this.idMap, args ).exec();
			case "last":
				return new Task_ListLast( dsm, this.idMap, args ).exec();
			case "list":
				return new Task_ListTags( dsm, this.idMap, args ).exec();
			case "smallest":
				return new Task_ListSize( dsm, this.idMap, args, true ).exec();
			case "biggest":
				return new Task_ListSize( dsm, this.idMap, args, false ).exec();
			case "tree":
				return new Task_Tree( dsm, this.idMap, args ).exec();
			case "stores":
				return new Task_ListStores( dsm, this.idMap, args ).exec();
			case "status": // size
				return new Task_Size( dsm, this.idMap, args ).exec();
			case "on":
				return new Task_OnOff( dsm, args, true ).exec();
			case "off":
				return new Task_OnOff( dsm, args, false ).exec();

			case "o":
			case "open":
				return this.iTaskOpen(dsm, args);
			case "show":
				return this.iTaskShow(dsm, args);
			case "describe":
				return new Task_Describe( dsm, this.idMap, args ).exec();

			case "info":
			case "meta":
				return new Task_MetaInfo( dsm, this.idMap, args, null ).exec();
			case "tags":
				return new Task_MetaInfo( dsm, this.idMap, args, META_TYPE.TAGS ).exec();
			case "hits":
				return new Task_MetaInfo( dsm, this.idMap, args, META_TYPE.HITS ).exec();
			case "created":
				return new Task_MetaInfo( dsm, this.idMap, args, META_TYPE.CREATED ).exec();
			case "changed":
				return new Task_MetaInfo( dsm, this.idMap, args, META_TYPE.CHANGED ).exec();
			case "title":
				return new Task_MetaInfo( dsm, this.idMap, args, META_TYPE.TITLE ).exec();
//			case "trash":
//				return new Task_MetaInfo(dsm, args, this.lastID, META_TYPE.TRASH).exec();

//			case "r":
//			case "sreg":
//			case "regex":
//				throw Err.todo(args[0].toLowerCase());

			case "e":
			case "edit":
				return new Task_Edit( dsm, this.idMap, args, false ).exec();
			case "E":
			case "Edit":
				return new Task_Edit( dsm, this.idMap, args, true ).exec();
//			case "r":
			case "rename":
				return new Task_Rename( dsm, this.idMap, args, false ).exec();
//			case "R":
			case "Rename":
				return new Task_Rename( dsm, this.idMap, args, true ).exec();

			case "n":
			case "new":
				return new Task_New( dsm, args, false ).exec();
			case "N":
			case "New":
				return new Task_New( dsm, args, true ).exec();

			case "d":
			case "del":
//			case "delete":
				// 1. Move to trash
				// 2. Delete it really
				return new Task_Delete( dsm, this.idMap, args ).exec();

			case "u":
			case "undel":
//			case "undelete":
				return new Task_UnDelete( dsm, this.idMap, args ).exec(); // from Trash

			case "t":
//			case "st":
//			case "stag":
//			case "stags":
//			case "tall":
			case "tag":
				return new Task_SearchTags( dsm, this.idMap, args, false ).exec();
			case "T":
//			case "stags":
//			case "ST":
//			case "tany":
			case "Tag":
				return new Task_SearchTags( dsm, this.idMap, args, true ).exec();

//			case "t":
//			case "g":
//			case "group":
//			case "l":
//			case "link":
//			case "tag":
			case "c":
			case "class":
//			case "classify": // = klassifizieren, einordnen
//			case "categorize":
				return new Task_Classify( dsm, this.idMap, args, false ).exec();
//			case "G":
//			case "Group":
//			case "L":
//			case "Link":
			case "C":
			case "Class":
//			case "Classify":
				return new Task_Classify( dsm, this.idMap, args, true ).exec();
			case "merge":
				return new Task_Merge( dsm, this.idMap, args ).exec();

			case "v":
			case "version":
				return new Task_Version( dsm, this.idMap, args ).exec();

			case "x":
			case "trash":
				return new Task_ListTrash( dsm, args ).exec();

			case "X":
			case "clean":
				return new Task_Clean( dsm, args ).exec();

			case "import":
				return new Task_Import( dsm, this.idMap, args ).exec();

			case "export":
				return new Task_Export( dsm, this.idMap, args ).exec();

			case "mv":
			case "move":
				return new Task_CopyMove( dsm, this.idMap, args, true ).exec();
			case "cp":
			case "copy":
				return new Task_CopyMove( dsm, this.idMap, args, false ).exec();

			case "clone":
				return new Task_Clone( dsm, this.idMap, args ).exec();

			case "combine":
				return new Task_Combine( dsm, this.idMap, args ).exec();

			case "diff":
				return new Task_Diff( dsm, this.idMap, args, false ).exec();
			case "Diff":
				return new Task_Diff( dsm, this.idMap, args, true ).exec();

			case "h":
			case "help":
				return new Task_Help( dsm, this.idMap, args, true ).exec();
			case "man":
			case "manual":
				return new Task_Help( dsm, this.idMap, args, false ).exec();

			case "-":
				return new Task_Unmap( dsm, this.idMap, args ).exec();

			default:
				return new Task_Help( dsm, this.idMap, null, true ).exec();
		}
	}

	/**
	 * @implSpec Run default task. Default is "show"
	 */
	private IdMap iTaskDefault( I_DataStoreManager dsm, String[] args ) {
		TASK_DEFAULT which = null;

		if(this.override != null)
			which = this.override.defaultTask();
		if(which == null)
			which = TASK_DEFAULT.SHOW;

		switch(which) {
			case SHOW:
				return this.iTaskShow(dsm, args);
			case OPEN:
				return this.iTaskOpen(dsm, args);
			default:
				throw Err.impossible( which );
		}
	}

	/**
	 * @implSpec If override exists, run "open" from override, otherwise run "show"
	 */
	private IdMap iTaskOpen( I_DataStoreManager dsm, String[] args ) {
		IdMap result = null;

		if(this.override != null)
			result = this.override.runOpen( dsm, this.idMap, args );

		if(result == null)
			result = this.iTaskShow( dsm, args );

		return result;
	}

	private IdMap iTaskShow( I_DataStoreManager dsm, String[] args  ) {
		return new Task_Show( dsm, this.idMap, args ).exec();
	}

}
