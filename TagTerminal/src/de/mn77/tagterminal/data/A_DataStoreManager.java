/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data;

import java.util.Collection;
import java.util.Map;

import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.tagterminal.config.TagLinks;
import de.mn77.tagterminal.store.I_Store;


/**
 * @author Michael Nitsche
 * @created 08.02.2021
 */
public abstract class A_DataStoreManager implements I_DataStoreManager {

	private I_TT_Config  config   = null;
	private TagLinks     tagLinks = null;
	private final String configName;


	public A_DataStoreManager( final String configName ) {
		this.configName = configName;
	}


	@Override
	public void close() {
		for( final I_Store store : this.getDataStoreMap().values() )
			store.close();

		try {
			this.tagLinks.write();
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

//	public boolean ready() {
//		return DBManager.instanz.gVerbunden();
//		return stores != null;
//	}

	@Override
	public void connect() throws Exception {
		if( this.config != null )
			new Err_Runtime( "Allready connected!" );

		this.config = this.pNewConfig( this.configName );
		this.config.read();

//		PageID.setDefaultWidth(this.config.getDefaultPageIdLength());

		for( final I_Store store : this.config.getStoreMap().values() )
			store.connect();

		this.tagLinks = new TagLinks( this.config.getConfigLinkDir() );
		this.tagLinks.read();
	}

	public I_Table<String> getAllTagLinks() {
		return this.tagLinks.getAll();
	}

	@Override
	public Map<Integer, I_Store> getDataStoreMap() {
		if( this.config == null )
			new Err_Runtime( "Not connected!" );
		return this.config.getStoreMap();
	}

	@Override
	public Map<Integer, I_Store> getDataStoreMapFull() {
		if( this.config == null )
			new Err_Runtime( "Not connected!" );
		return this.config.getStoreMapFull();
	}

	/**
	 * Result can be null
	 */
	@Override
	public I_Store getDefaultStore() {
		if( this.config == null )
			return null;
		return this.config.getWriteStore( true );
	}

	public String getEditEditor() {
		return this.config.getEditEditor();
	}

	public String getEditEditorX() {
		return this.config.getEditEditorX();
	}

	@Override
	public boolean getExperimental() {
		return this.config.getExperimental();
	}

	public String getShowFormat() {
		return this.config.getShowFormat();
	}

	public String getShowViewer() {
		return this.config.getShowViewer();
	}

	@Override
	public Collection<String> getTagLinks( final String tag ) {
		return this.tagLinks.linkedWith( tag );
	}

	/**
	 * Throws error when no store available
	 */
	@Override
	public I_Store getWriteStore() {
		if( this.config == null )
			new Err_Runtime( "Not connected!" );
		return this.config.getWriteStore( false );
	}

	@Override
	public boolean isSingleStore() {
		return this.getDataStoreMap().size() == 1;
	}

	@Override
	public void linkTags( final String base, final String target ) {

		try {
			this.tagLinks.link( base, target );
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

	public abstract I_TaskTool tool();

	@Override
	public boolean unlinkTags( final String base, final String target ) {

		try {
			return this.tagLinks.unlink( base, target );
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
			return false;
		}
	}

	protected abstract I_TT_Config pNewConfig( String configName2 );

}
