/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data;

import java.util.Collection;
import java.util.Map;

import de.mn77.base.data.struct.table.I_Table;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.store.I_Store;


/**
 * @author Michael Nitsche
 * @created 24.09.2022
 */
public interface I_DataStoreManager {

	void close();

	void connect() throws Exception;

	/**
	 * Contains only enabled stores
	 */
	Map<Integer, I_Store> getDataStoreMap();

	Map<Integer, I_Store> getDataStoreMapFull();

	/**
	 * Result can be null
	 */
	I_Store getDefaultStore();

	String getEditEditor();

	String getEditEditorX();

	boolean getExperimental();

	String getShowFormat();

	String getShowViewer();

	Collection<String> getTagLinks( String tag );

	/**
	 * Throws error when no store available
	 */
	I_Store getWriteStore();

	boolean isSingleStore();

	void linkTags( String base, String target );

	I_TaskTool<I_ItemData> tool();

	boolean unlinkTags( String base, String target );

	I_Table<String> getAllTagLinks();

}
