/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data;

import java.io.File;

import de.mn77.base.data.group.Group2;
import de.mn77.base.sys.file.MDir;
import de.mn77.tagterminal.data.container.I_ItemData;


/**
 * @author Michael Nitsche
 * @created 24.09.2022
 */
public interface I_TaskTool<TI extends I_ItemData> {

	TI clone( TI s );

	TI combine( TI s1, TI s2 );

	String convert( TI item, OUTPUT_FORMAT form );

	void diff( TI page1, TI page2, boolean gui );

	Group2<EDIT_RESULT, TI> edit( TI s, boolean gui );

	Group2<EDIT_RESULT, I_ItemData> rename( TI s, boolean gui );

	void export( MDir dir, TI page );

	String getPrefix();

	Object getVersion();

//	void show(TI page);

	TI importItem( File ile );

	TI newItem( boolean gui );

}
