/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data;

import java.io.File;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.tagterminal.data.container.I_ItemData;


/**
 * @author Michael Nitsche
 */
public class ItemView {

	public static final String[] VIEWERS = { "default", "direct", "pager", "less", "more", "most", "lynx", "links", "xbrowser" };
	public static final String[] FORMATS = { "default", "wiki", "color", "html" };

	private final I_TaskTool<I_ItemData> tool;


	public ItemView( final I_TaskTool<I_ItemData> tool ) {
		this.tool = tool;
	}


	public void show( final I_ItemData item, final String format, final String viewer ) {
		String suffix = ".txt"; // default
		String output = null;

		switch( format ) { // TODO search
			case "color":
				output = this.tool.convert( item, OUTPUT_FORMAT.COLOR );
				break;
			case "wiki":
				output = this.tool.convert( item, OUTPUT_FORMAT.WIKI );
				break;
			case "html":
				output = this.tool.convert( item, OUTPUT_FORMAT.HTML );
				suffix = ".html";
				break;

			case "default":
//			case "raw":
			case "text":
			default:
				output = this.tool.convert( item, OUTPUT_FORMAT.TEXT );
				break;
		}

		switch( viewer.toLowerCase() ) {
			case "direct":
				MOut.print( output );
				break;

			case "default":
			case "pager":
				this.iOutput( output, "pager", true, suffix );
				break;
			case "less":
				this.iOutput( output, "less -R", true, suffix );
				break;
			case "xbrowser":
				this.iOutput( output, "xdg-open", true, suffix );
				break;

			default:
				this.iOutput( output, viewer, true, suffix );
				break;
		}
	}

	private void iOutput( final String s, final String cmd, final boolean wait, final String suffix ) {

		try {
			final File tmpFile = File.createTempFile( this.tool.getPrefix(), suffix );
			Lib_TextFile.set( tmpFile, s );
			SysCmd.exec( true, wait, cmd, tmpFile.getAbsolutePath() );
			if( wait )
				tmpFile.delete();
		}
		catch( final Exception e ) {
			Err.exit( e );
		}
	}

//	private void output1(final PageData s, final String[] search) {
//
////		MOut.text(Lib_String.sequence('*', 78));
////		MOut.text("= "+s.title+" =");
////		MOut.text(s.text);
////		MOut.text(Lib_String.sequence('*', 78));
//
//		MOut.print(Lib_String.sequence('-', 78));
//		MOut.print("= " + s.getTitle());
//
//		final List<String> lines = ConvertString.toLines(s.text);
//		String headline = null;
//		int headlineDepth = 0;
//		StringBuilder sb = new StringBuilder();
//
//		for(int i = 0; i < lines.size(); i++) {
//			final String line = lines.get(i);
//			final char lineStyle = s.lineStyles == null
//				? ' '
//				: s.lineStyles.charAt(i);
//			if(lineStyle == ' ' || lineStyle == 'c')
//				sb.append(line + '\n');
//			else {
//				this.output2(headline, headlineDepth, sb.toString(), search);
//				sb = new StringBuilder();
//				headline = line;
//				headlineDepth = Integer.parseInt("" + lineStyle);
//			}
//		}
//
//		this.output2(headline, headlineDepth, sb.toString(), search);
//
//		MOut.print(Lib_String.sequence('-', 78));
//	}
//
//	private void output2(final String headline, final int headlineDepth, final String block, final String[] search) {
////		MOut.temp(search, block.length());
//
//		if(block.length() == 0)
//			return;
//
//		if(search == null || search.length == 0)
//			this.output3(headline, headlineDepth, block);
//		else
//			for(final String s : search)
//				if(block.toLowerCase().contains(s.toLowerCase())) {
////					block = block.replaceAll(s, "!!!"+s.toUpperCase()+"!!!");
//					this.output3(headline, headlineDepth, block);
//					return;
//				}
//	}
//
//	private void output3(final String headline, final int headlineDepth, final String block) {
//		if(headline != null) {
//			final String headlineIdent = Lib_String.sequence('=', headlineDepth);
//			final String hl = headlineIdent + " " + headline + " " + headlineIdent;
//			MOut.print(hl);
//		}
//		else
//			MOut.print("===");
//
//		MOut.print(FilterString.trimNewLines(block, true, true));
//	}

}
