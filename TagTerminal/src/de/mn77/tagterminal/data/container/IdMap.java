/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.mn77.base.data.form.FormNumber;
import de.mn77.base.error.Err;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.util.Lib_Comply;


/**
 * @author Michael Nitsche
 * @created 20.09.2022
 */
public class IdMap implements Iterable<SearchResultEntry> {

	private String                                         last = null;
	private final HashMap<Integer, List<SearchResultItem>> map;
	private boolean                                        hasTags = false;


	/**
	 * @apiNote Create empty map
	 */
	public IdMap() {
		this.map = new HashMap<>( 10 );
	}

	/**
	 * @apiNote Copy map
	 */
	public IdMap( final IdMap old ) {
		this();
		Err.ifNull( old );
		this.hasTags = old.hasTags;

		for( final Entry<Integer, List<SearchResultItem>> e : old.map.entrySet() )
			this.map.put( e.getKey(), e.getValue() );

		this.setSingleLastID();
	}

	/**
	 * @apiNote Copy map, lock id
	 * @param lastMemID (null, 150, 1#23)
	 */
	public IdMap( final IdMap old, final String lastMemId ) {
		this( old );

		if( lastMemId != null )
			// correct width
			this.last = lastMemId.charAt( 0 ) + FormNumber.width( this.iMaxWidth(), lastMemId.substring( 1 ), false );
	}

	/**
	 * @apiNote Create new map with only one single ID
	 */
	public IdMap( final ItemID newID, final String title, final String[] tags ) {
		this();

		this.hasTags = tags != null;

		if( newID != null ) {
			final SearchResultItem dsri = new SearchResultItem( newID, title, tags );
			this.put( newID.storeID, new SearchResult(dsri) );
			this.setSingleLastID();
		}
	}

	public String computeLastID() {
		Err.ifNull( this.last );
		return this.last;
	}

//	public PageID getPageID() {
//		Err.ifNull(this.last);
//		return this.getPageID(this.last);
//	}

	public Map<String, Integer> computeMap() {
		final int maxWidth = this.iMaxWidth();
		final HashMap<String, Integer> map = new HashMap<>();

		for( final Entry<Integer, List<SearchResultItem>> e : this.map.entrySet() )
			for( int i = 0; i < e.getValue().size(); i++ ) {
				final int mid = Integer.parseInt( "" + e.getKey() + FormNumber.width( maxWidth, i, false ) );
				map.put( e.getValue().get( i ).getID().getAbsolute(), mid );
			}

		return map;
	}

	public ItemID getItemID( final String id ) {
		final boolean isAbsolute = Lib_Comply.checkAbsoluteID( id );
		final int storeID = id.charAt( 0 ) - '0';

		if( isAbsolute ) {
			final int internalID = Integer.parseInt( id.substring( 2 ) );
			return new ItemID( storeID, internalID );
		}
		else {
			final int y = Integer.parseInt( id.substring( 1 ) );

			if( !this.map.containsKey( storeID ) )
				throw new TagTerminalException( "Invalid ID: " + id );

			final List<SearchResultItem> list = this.map.get( storeID );
			if( list.size() <= y )
				throw new TagTerminalException( "Invalid ID: " + id );

			return list.get( y ).getID();
		}
	}

	public boolean hasContent() {
		return this.map.size() > 0;
	}

	public boolean hasOnlyOne() {
		return this.size() == 1;
	}

//	public void setLast(final String mid) {
//		if(this.map.size() == 0)
//			throw new Err_Runtime("Got ID without map!");
//
//		this.last = mid.charAt(0)+FormNumber.width(this.iMaxWidth(), mid.substring(1), false);
////		this.last = mid;
//	}

	public boolean hasSelection() {
//		return this.last != null && this.map.size() > 0;
		return this.last != null;
	}

	public boolean hasTags() {
		return this.hasTags;
	}

	public Iterator<SearchResultEntry> iterator() {
		final ArrayList<SearchResultEntry> al = new ArrayList<>();
		final int maxWidth = this.iMaxWidth();

		for( final Entry<Integer, List<SearchResultItem>> e : this.map.entrySet() )
			for( int i = 0; i < e.getValue().size(); i++ ) {
				final int mid = Integer.parseInt( "" + e.getKey() + FormNumber.width( maxWidth, i, false ) );
				final SearchResultEntry g = new SearchResultEntry( mid, e.getValue().get( i ) );
				al.add( g );
			}

		return al.iterator();
	}

	public String prompt( final String empty, final String map ) {
		return this.last != null
			? this.last
			: this.size() == 0
				? empty
				: map;
	}

	public void put( final int store, final SearchResult result ) {
		this.map.put( store, result.getItems() );

		if( result.hasTags() )
			this.hasTags = true;
	}

	public void put( final int store, final List<SearchResultItem> items, final boolean hasItems ) {
		this.map.put( store, items );

		if( hasItems )
			this.hasTags = true;
	}

	public void setSingleLastID() {
		if( this.last == null && this.hasOnlyOne() )
			for( final Entry<Integer, List<SearchResultItem>> e : this.map.entrySet() )
				if( e.getValue().size() == 1 ) {
					this.last = "" + e.getKey() + "0";
					return;
				}
	}

	public int size() {
		int amount = 0;

		for( final List<SearchResultItem> l : this.map.values() )
			amount += l.size();

		return amount;
	}

	private int iMaxWidth() {
		int storeMax = 0;
		for( final List<SearchResultItem> l : this.map.values() )
			if( l.size() > storeMax )
				storeMax = l.size();

		return ("" + storeMax).length();
	}

}
