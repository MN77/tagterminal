/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 01.03.2022
 */
public class ItemAccess {

	private final ItemID  id;
	private final I_Store store;
	private final boolean writeable;


	public ItemAccess( final I_DataStoreManager dsm, final IdMap map, final String id, final boolean writeAccess ) {
		this( dsm, map.getItemID( id ), id, writeAccess );
	}

	public ItemAccess( final I_DataStoreManager dsm, final ItemID id, final String origin, final boolean writeAccess ) {
		final I_Store store = dsm.getDataStoreMap().getOrDefault( id.storeID, null );
		this.iCheckPageID( store, id, origin );

		if( store == null )
			throw new TagTerminalException( "Invalid ID: " + id );

		if( writeAccess )
			Lib_Task.checkWrite( store );

		this.store = store;
		this.writeable = writeAccess;
		this.id = id;
	}

	public ItemID getID() {
		return this.id;
	}

	public I_Store getStore() {
		return this.store;
	}

	public boolean getWriteable() {
		return this.writeable;
	}

	private void iCheckPageID( final I_Store store, final ItemID pid, final String originalID ) {
		// TODO Move to Lib_Comply ?!?

		if( store == null || pid == null || !store.items().isIdValid( pid ) ) {
			final String idString = this.id != null
				? this.id.getAbsolute()
				: originalID;
			throw new TagTerminalException( "Invalid ID: " + idString );
		}
	}

}
