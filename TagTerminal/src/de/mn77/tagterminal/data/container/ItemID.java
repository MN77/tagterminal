/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import de.mn77.base.error.Err;
import de.mn77.tagterminal.util.Lib_ItemID;


/**
 * @author Michael Nitsche
 * @created 03.03.2022
 */
public class ItemID {

	public final int internalID;
	public final int storeID;


	public ItemID( final int storeID, final int internalID ) {
		Err.ifOutOfBounds( 1, 9, storeID );
		Err.ifTooSmall( 0, internalID );

		this.storeID = storeID;
		this.internalID = internalID;
	}

	public String getAbsolute() {
		return Lib_ItemID.encodeAbsolute( this.storeID, this.internalID ); // , null
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '(' );
		sb.append( "storeID=" );
		sb.append( this.storeID );
		sb.append( ',' );
		sb.append( "internalID=" );
		sb.append( this.internalID );
		sb.append( ',' );
		sb.append( "absolute=" );
		sb.append( this.getAbsolute() );
		sb.append( ')' );
		return sb.toString();
	}

}
