/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import de.mn77.base.data.datetime.I_DateTime;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class ItemMetaData extends SearchResultItem {

	private final I_DateTime changed;
	private final I_DateTime created;
	private final int        hits;
	private final long       size;
//	public final boolean crypt;
	private final boolean trash;


	public ItemMetaData( final ItemID id, final String title, final String[] tags, final int hits, final I_DateTime created, final I_DateTime changed, final boolean trash, final long size ) {
		super( id, title, tags );
		this.hits = hits;
		this.created = created;
		this.changed = changed;
		this.trash = trash;
		this.size = size;
//		this.crypt = crypt;
	}

	public ItemMetaData( ItemMetaData i ) {
		this( i.getID(), i.getTitle(), i.getTags(), i.getHits(), i.getCreated(), i.getChanged(), i.getTrash(), i.getSize() );
	}


	public I_DateTime getChanged() {
		return this.changed;
	}

	public I_DateTime getCreated() {
		return this.created;
	}

	public int getHits() {
		return this.hits;
	}

	public boolean getTrash() {
		return this.trash;
	}

	public long getSize() {
		return this.size;
	}

	public boolean isNewItem() {
		return this.getID() == null;
	}

}
