/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import java.util.Iterator;
import java.util.List;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 26.11.2024
 */
public class SearchResult implements Iterable<SearchResultItem> {

	private final boolean                hasTags;
	private final List<SearchResultItem> items;


	public SearchResult( final boolean hasTags ) {
		this.items = new SimpleList<>();
		this.hasTags = hasTags;
	}

	public SearchResult( final SearchResultItem dsri ) {
		this.items = new SimpleList<>();
		this.hasTags = dsri.getTags() != null;
		this.add( dsri );
	}


	public void add( final SearchResultItem item ) {
		this.items.add( item );
	}

	public List<SearchResultItem> getItems() {
		return this.items;
	}

	public boolean hasTags() {
		return this.hasTags;
	}

	public Iterator<SearchResultItem> iterator() {
		return this.items.iterator();
	}

}
