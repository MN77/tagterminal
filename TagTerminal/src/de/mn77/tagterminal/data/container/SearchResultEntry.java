/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import de.mn77.base.data.convert.ConvertObject;


/**
 * @author Michael Nitsche
 * @created 13.02.2023
 */
public class SearchResultEntry {

	public final int              key;
	public final SearchResultItem item;


	public SearchResultEntry( final int key, final SearchResultItem item ) {
		this.key = key;
		this.item = item;
	}


	@Override
	public String toString() {
		return "SearchResultEntry(" + ConvertObject.toStringOutput( this.key ) + "," + ConvertObject.toStringOutput( this.item ) + ")";
	}

}
