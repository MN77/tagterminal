/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.data.container;

import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err;

/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class SearchResultItem {

	private final ItemID id;
	private String title;
	private final String[] tags;


	public SearchResultItem( final ItemID id, final String title, String[] tags ) {
		Err.ifNull( title );
		this.id = id;
		this.title = FormString.limit( title, 255, "..." );
		this.tags = tags; // Can be null
	}

	public ItemID getID() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle( String s ) {
		this.title = s;
	}

	public String toIdent() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( ':' );
		sb.append( '<' );
		sb.append( this.id.getAbsolute() );
		sb.append( '>' );
		sb.append( this.title );
		return sb.toString();
	}

	/**
	 * @return can be null
	 */
	public String[] getTags() {
		return this.tags;
	}

}
