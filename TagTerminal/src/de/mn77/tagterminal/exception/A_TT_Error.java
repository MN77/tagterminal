/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.exception;

import de.mn77.base.error.Lib_Exception;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2022-12-04
 */
public abstract class A_TT_Error extends RuntimeException {

	private static final long serialVersionUID = -6007573625554480033L;
	private final String name;


	public A_TT_Error( String name ) {
		super( "Internal "+name+" error!" );
		this.name = name;
	}

	public A_TT_Error( String name, final String message ) {
		super( message );
		this.name = name;
	}

	public A_TT_Error( String name, final Throwable t ) {
		super( Lib_Exception.firstMessage( t ), t );
		this.name = name;
	}


	public void show() {
		MOut.print( "--- Error ---", this.getMessage(), this.name + " will now be terminated. Sorry!" );
		System.exit( 2 );
	}

	public void show( Throwable t ) {
		MOut.error( t );

		while( t.getCause() != null ) {
			t = t.getCause();
			MOut.error( t );
		}

		this.show();
	}

}
