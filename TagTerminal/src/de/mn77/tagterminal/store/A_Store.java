/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store;

/**
 * @author Michael Nitsche
 * @created 07.12.2021
 */
public abstract class A_Store implements I_Store {

	protected final String[] loginData;
	private final int        storeID;
	private boolean          enabled = true;


	public A_Store( final int storeID, final String[] login ) {
		this.storeID = storeID;
		this.loginData = login;
	}

	public final void connect() throws Exception {
		this.connect( this.loginData );
	}

	public final int getStoreID() {
		return this.storeID;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled( boolean b ) {
		this.enabled = b;
	}

	protected abstract void connect( String[] login ) throws Exception;

}
