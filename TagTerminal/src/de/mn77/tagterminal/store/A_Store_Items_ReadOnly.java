/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.ItemID;


/**
 * @author Michael Nitsche
 * @created 21.02.2022
 */
public abstract class A_Store_Items_ReadOnly<TI extends I_ItemData> implements I_Store_Items<TI> {

	public final ItemID add( final TI s ) {
		MOut.print( "This store is readonly!" );
		throw Err.forbidden( s );
	}

	public final void delete( final ItemID id ) {
		MOut.print( "This store is readonly!" );
		Err.forbidden( id );
	}

	public void deleteAll() {
		MOut.print( "This store is readonly!" );
		Err.forbidden();
	}

	public final void moveToTrash( final ItemID id ) {
		MOut.print( "This store is readonly!" );
		Err.forbidden( id );
	}

	public final void unTrash( final ItemID id ) {
		MOut.print( "This store is readonly!" );
		Err.forbidden( id );
	}

	public final void update( final TI s ) {
		MOut.print( "This store is readonly!" );
		Err.forbidden( s );
	}

}
