/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store;

import de.mn77.tagterminal.data.container.ItemID;


/**
 * @author Michael Nitsche
 * @created 31.01.2021
 */
public interface I_Store {

	boolean canHits();

	boolean canTags();

	boolean canTrash();

	boolean canWrite();

	void close();

	void connect() throws Exception;

	void delete( ItemID id );

	@Deprecated
	void deleteAll();

	String getInfo();

	int getStoreID();

	String getType();

//	I_DataStore_Tree storeTree();	// Tags

//	I_DataStore_Config storeConfig();

	I_Store_Items items();

	I_Store_Tags tags();

	boolean isEnabled();

	void setEnabled( boolean b );

}
