/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store;

import java.util.Collection;
import java.util.List;

import de.mn77.base.error.Err_FileSys;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResult;


/**
 * @author Michael Nitsche
 * @created 31.01.2021
 */
public interface I_Store_Items<TI extends I_ItemData> {

	/**
	 * @return Returns the new page ID. If the page could not be added, the result should be null.
	 * @throws Err_FileSys
	 */
	ItemID add( TI item ) throws Err_FileSys;

	List<TI> getAllItems();

	Collection<ItemMetaData> getAllMetaData(); // Currently not really used

	TI getItem( final ItemID id, boolean count );

	int getSize( boolean trash );

	boolean isIdValid( ItemID id );

	boolean isInTrash( ItemID id );

	void moveToTrash( ItemID id );

	/**
	 * @apiNote The result can be smaller, greater and in different order then requested.
	 * @return Returns a list which should, but does not have to, match the given arguments.
	 **/
	Collection<ItemMetaData> requestMetaData( ITEM_META_TYPE sort, boolean asc, int requestedAmount );

	SearchResult search( String[] search, boolean or, boolean onlyTitle );

	SearchResult searchAll();

//	List<Integer> getAllIDs();

	SearchResult searchTags( String[] searchTags, boolean or ); // TODO in tags verschieben ?!?

	SearchResult trash();

	void unTrash( ItemID id );

	void update( TI s );

}
