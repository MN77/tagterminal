/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store.mariadb;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_DataBase;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.login.Login_MariaDB;
import de.mn77.ext.db.sql.MariaDB;
import de.mn77.ext.db.util.Lib_DBInfoTable;
import de.mn77.ext.db.util.Lib_SqlDB;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.A_Store;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;
import de.mn77.tagterminal.util.Lib_DBVersion;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public abstract class A_Store_MariaDB<TI extends I_ItemData> extends A_Store {

	private static final int DB_VERSION_CREATE = 1;

	private I_Store_Items<TI> tableItems;
	private I_Store_Tags      tableTags;
	private MariaDB           db = null;
	private final String      dbIdent;
	private final String[]    dbIdentOld;


	/**
	 * loginData = host,port,user,pass,db // host,user,pass,db
	 */
	public A_Store_MariaDB( final int storeID, final String[] loginData, final String dbIdent, final String[] dbIdentOld ) {
		super( storeID, loginData );
		this.dbIdent = dbIdent;
		this.dbIdentOld = dbIdentOld;
	}


	public boolean canHits() {
		return true;
	}

	public boolean canTags() {
		return true;
	}

	public boolean canTrash() {
		return true;
	}

	public boolean canWrite() {
		return true;
	}

	public void close() {
		this.db.close();
		this.db = null;
	}

	public void delete( final ItemID id ) {
		// --- Declassify ---
		final String[] itemTags = this.tableTags.getClasses( id );
		Err.ifNull( itemTags );

		for( final String tag : itemTags )
			this.tableTags.declassify( id, tag );

		// Bad ... USES will not be updated!
//		final String sql1 = "DELETE FROM tagmap WHERE item_id=" + id.internalID;
//		this.db.update(sql1);

		final String sql2 = "DELETE FROM items WHERE id=" + id.internalID;
		final int changed = this.db.update( sql2 );

		if( changed != 1 )
			throw new Err_Runtime( "Task could not be deleted!" );
	}

	public void deleteAll() {
		this.db.update( "DELETE FROM tagmap" );
		this.db.update( "DELETE FROM items" );
		this.db.update( "DELETE FROM tags" );
	}

	public String getInfo() {
		return super.loginData[0] + ':' + super.loginData[3];
	}

	public String getType() {
		return "MariaDB";
	}

	public I_Store_Items<TI> items() {
		return this.tableItems;
	}

	public I_Store_Tags tags() {
		return this.tableTags;
	}

	@Override
	protected void connect( final String[] login ) throws Exception {
		this.db = new MariaDB();
		final Login_MariaDB dbLogin = Login_MariaDB.from( login );
		this.db.connect( dbLogin, false );

		// "dbinfo" nutzen, andere Tabellen können sich mit "update" ändern!
		final boolean exist = Lib_SqlDB.existTable( this.db, "dbinfo" );

		if( !exist ) {
			MOut.print( "Initializing database: " + dbLogin, "Please wait ..." );

			try {
				this.iCreate();
			}
			catch( final Exception e ) {
				throw new Err_DataBase( e, "Database could not be created", dbLogin );
			}
			MOut.print( "Database initialized" );
		}
		else {
			if( !this.iCheckCorrectDB() )
				throw new Err_DataBase( "Invalid database for this programm: " + dbLogin );

			this.iUpdateProcess( dbLogin.toString() );
		}

//		this.tableTags = this.pNewTags(this.db);
		this.tableTags = new Store_MariaDB_Tags( this.db );
		this.tableItems = this.pNewItems( this.getStoreID(), this.db, this.tableTags );
	}

	protected abstract String[] pCreateIndexes();

	protected abstract String pCreateItemFields();

	protected abstract I_Store_Items<TI> pNewItems( int storeID, MariaDB db2, I_Store_Tags tableTags2 );

	protected abstract Integer pUpdateMinor( MariaDB db, int oldVersion );

	private boolean iCheckCorrectDB() {
		return Lib_DBInfoTable.checkIdent( this.db, this.dbIdent, this.dbIdentOld );
	}

	private void iCreate() {
//		"id,created,changed,crypt,trash,clicks,title,text,linestyles,textstyles"

		Lib_DBInfoTable.create( this.db, this.dbIdent );
//		(crypt)
//		passhash
//		passdb


		final String sqlItems = "" +
			"CREATE TABLE items(" +
			"id INTEGER NOT NULL AUTO_INCREMENT," +
			"created BIGINT NOT NULL," +
			"changed BIGINT NOT NULL," +
//			"crypt INTEGER NOT NULL," +
			"trash INTEGER NOT NULL," +
			"hits INTEGER NOT NULL," +
			"title VARCHAR(255) NOT NULL," +
			this.pCreateItemFields() +
			"PRIMARY KEY(id))";

//		MOut.print(sql);
		this.db.exec( sqlItems );

		for( final String sql : this.pCreateIndexes() )
			this.db.exec( sql );

//		this.db.exec("CREATE INDEX i_trash ON pages (trash)");
//		this.db.exec("CREATE INDEX i_text ON pages (trash,title,crypt,text)");
//		this.db.exec("CREATE INDEX i_created ON pages (created DESC)");
//		this.db.exec("CREATE INDEX i_changed ON pages (changed DESC)");
//		this.db.exec("CREATE INDEX i_hits ON pages (hits DESC)");

		final String sqlTags = "" +
			"CREATE TABLE tags(" +
			"id INTEGER NOT NULL AUTO_INCREMENT," +
			"name VARCHAR(32) NOT NULL," +
			"uses INTEGER NOT NULL," +	//  Used for sorting tags by popularity
			"PRIMARY KEY(id)" +
			")";
//		MOut.print(sql);
		this.db.exec( sqlTags );
		this.db.exec( "CREATE INDEX i_name ON tags (name)" );

		final String sqlTagItem = "" +
			"CREATE TABLE tagmap(" +
			"tag_id INTEGER NOT NULL," +
			"item_id INTEGER NOT NULL," +
			"FOREIGN KEY (tag_id) REFERENCES tags(id) ON DELETE RESTRICT," +
			"FOREIGN KEY (item_id) REFERENCES items(id) ON DELETE RESTRICT," +
			"UNIQUE(tag_id,item_id)" +
			")";
		this.db.exec( sqlTagItem );

		final int initVersion = Lib_DBVersion.encode( A_Store_MariaDB.DB_VERSION_CREATE, 1 );
		Lib_DBInfoTable.setVersion( this.db, initVersion );
	}

	private Integer iUpdateMajor( final MariaDB db2, final int version ) {
//		if( version < 2 ) {
//			this.db.exec( "RENAME TABLE tag_page TO tagmap" );
//			this.db.exec( "RENAME TABLE pages TO items" );
//			this.db.exec( "ALTER TABLE tagmap RENAME COLUMN page_id TO item_id" );
//			version = 2;
//		}

//		return version;
		return null;
	}

	private void iUpdateProcess( final String storeIdent ) {
		int oldVersion = this.db.query_int( "SELECT value FROM dbinfo WHERE keyid='version'" );	// ident

		// Switch to new version system
		if( oldVersion < 1000 ) {
			oldVersion = Lib_DBVersion.encode( A_Store_MariaDB.DB_VERSION_CREATE, 1 );
			Lib_DBInfoTable.setVersion( this.db, oldVersion );
		}

		final int[] mm = Lib_DBVersion.decode( oldVersion );
		final int oldMajor = mm[0];
		final int oldMinor = mm[1];

		Integer newMajor = this.iUpdateMajor( this.db, oldMajor );
		if( newMajor == null )
			newMajor = 1;

		Integer newMinor = this.pUpdateMinor( this.db, oldMinor );
		if( newMinor == null )
			newMinor = 1;

		final int newVersion = Lib_DBVersion.encode( newMajor, newMinor );

		if( newVersion == oldVersion ) {}
		else {
			if( newMajor < oldMajor )
				throw new Err_DataBase( "Database version newer than TagTerminal: " + oldMajor + " > " + newMajor );
			if( newMinor < oldMinor )
				throw new Err_DataBase( "Database version newer than current software: " + oldMinor + " > " + newMinor );

			Lib_DBInfoTable.setVersion( this.db, newVersion );
			MOut.print( "Database updated to version " + newMajor + "/" + newMinor + ": " + storeIdent );
		}
	}

}
