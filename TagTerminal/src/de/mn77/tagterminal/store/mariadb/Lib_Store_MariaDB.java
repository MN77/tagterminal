/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store.mariadb;

import java.util.List;

import de.mn77.ext.db.sql.MariaDB;
import de.mn77.tagterminal.data.container.ItemID;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public class Lib_Store_MariaDB {

	public static String[] tags( final MariaDB db, final ItemID id ) {
//		String sql = "SELECT b.name FROM tag_page AS a JOIN tags AS b ON tag_page.tag_id = tags.id WHERE a.page_id = "+pageID;	// TODO fehlerhaft
//		String sql = "SELECT t.name FROM tags AS t JOIN tag_page AS tp ON tp.tag_id=t.id JOIN pages AS p ON p.id=tp.page_id WHERE p.id="+pageID;
//		String sql = "SELECT t.name FROM tags AS t JOIN tag_page AS tp ON tp.tag_id=t.id WHERE tp.page_id="+pageID;
//		final String sql = "SELECT t.name FROM tags AS t JOIN tag_page AS tp ON tp.tag_id=t.id WHERE tp.page_id=" + id.internalID + " ORDER BY uses DESC";
		final String sql = "SELECT t.name FROM tags AS t JOIN tagmap AS tp ON tp.tag_id=t.id WHERE tp.item_id=" + id.internalID + " ORDER BY uses DESC";
		final List<String> result = db.queryColumn( sql );
		return result.toArray( new String[result.size()] );
	}

}
