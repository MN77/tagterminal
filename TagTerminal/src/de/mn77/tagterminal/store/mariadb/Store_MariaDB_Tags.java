/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.store.mariadb;

import java.util.Map;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleMap;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.ext.db.sql.MariaDB;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store_Tags;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public class Store_MariaDB_Tags implements I_Store_Tags {

	private final MariaDB db;


	public Store_MariaDB_Tags( final MariaDB db ) {
		this.db = db;
	}


	public void classify( final ItemID id, final String tag ) {
		final String tagID = this.iFetchTagID( tag, true );

		String sql = "INSERT IGNORE INTO tagmap (tag_id,item_id) VALUES (" + tagID + "," + id.internalID + ")";
		this.db.update( sql );
		sql = "UPDATE tags SET uses=uses+1 WHERE id = " + tagID;
		this.db.update( sql );
	}

	public void declassify( final ItemID id, final String tag ) {
		final String tagID = this.iFetchTagID( tag, false );

		final StringBuilder sb = new StringBuilder();
		sb.append( "DELETE FROM tagmap WHERE tag_id=" );
		sb.append( tagID );
		sb.append( " AND item_id=" );
		sb.append( id.internalID );
		String sql = sb.toString();
		this.db.update( sql );

		sql = "UPDATE tags SET uses=uses-1 WHERE id = " + tagID;
		this.db.update( sql );

		// Maybe delete tag
		sql = "SELECT uses FROM tags WHERE id = " + tagID;
//		sql = "Select COUNT(item_id) AS uses FROM tagmap WHERE tag_id = " + tagID;
		final int uses = this.db.query_int( sql );

		if( uses == 0 ) {
			sql = "DELETE FROM tags WHERE id=" + tagID;
			this.db.update( sql );
		}
	}

	public I_List<String> getAllTags() {
		final String sql = "SELECT name FROM tags WHERE uses > 0 ORDER BY name";
		return this.db.queryColumn( sql );
	}

	public String[] getClasses( final ItemID id ) {
		return Lib_Store_MariaDB.tags( this.db, id );
	}

	public Map<String, Integer> getTagStats() {
		// All uses of a tag, they can be trashed:
//		final String sql = "SELECT name,uses FROM tags ORDER BY name"; // WHERE uses > 0
		// Only active uses of a tag:
		final String sql = "SELECT t.name AS name,COUNT(i.id) AS uses FROM tags AS t LEFT JOIN tagmap AS m ON m.tag_id = t.id LEFT JOIN items AS i ON i.id = m.item_id WHERE i.trash=false AND uses > 0 GROUP BY t.name;";
		final ArrayTable<Object> table = this.db.queryTableObject( sql );
		final Map<String, Integer> map = new SimpleMap<>();

		for( final Object[] row : table ) {
			final String tag = (String)row[0];
			final int uses = (int)(long)(Long)row[1];
			map.put( tag, uses );
		}

		return map;
	}

	private String iFetchTagID( final String tag, final boolean add ) {
		String sql = "SELECT id FROM tags WHERE name = '" + tag + "'";
		String tagID = this.db.queryString( sql );

		// Create tag
		if( tagID == null ) {
			if( !add )
				throw new TagTerminalException( "Unknown tag: " + tag );

			sql = "INSERT INTO tags (name,uses) VALUES ('" + tag + "',0)";
			this.db.update( sql );

			sql = "SELECT id FROM tags WHERE name = '" + tag + "'";
			tagID = this.db.queryString( sql );
		}

		return tagID;
	}

}
