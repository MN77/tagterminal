/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.base.MN;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 18.03.2022
 */
public abstract class A_Task {

	private final String[]           args;
	private final I_DataStoreManager dsm;
	private final IdMap              idMap;


	public A_Task( final I_DataStoreManager dsm, final IdMap map, final String[] input, final int min, final Integer max ) {
		this.dsm = dsm;
		this.idMap = map;
		this.args = Lib_Task.emptyInput( input )
			? new String[0]
			: U_StringArray.cutFrom( input, 1 );

		this.checkArguments( min, max );
	}

	public final IdMap exec() {
		return this.exec( this.dsm, this.idMap, this.args );
	}

	protected abstract IdMap exec( I_DataStoreManager dsm, IdMap idMap, String[] args );

	private void checkArguments( final int min, final Integer max ) { //, boolean reset
		final int len = this.args.length;
		String message = null;

		if( len < min )
			if( max == null )
				message = "Need " + min + " or more argument(s), but got only " + len;
			else
				message = "Need " + min + " argument(s), but got " + len;

		if( message == null && max != null && len > max )
			message = String.format( "Too many arguments! Only %d %s needed, but got %d", max, MN.numerus( max, "is", "are" ), len );

		if( message != null )
			throw new TagTerminalException( message );
	}

}
