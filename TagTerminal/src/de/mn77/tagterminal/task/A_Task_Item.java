/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;


/**
 * @author Michael Nitsche
 * @created 18.03.2022
 */
public abstract class A_Task_Item extends A_Task {

	private static String[] iArgumentsID( final IdMap map, String[] args, final int min, final boolean insertLastID ) {
		final int lenGot = args.length;

		if( map.hasSelection() && (lenGot < min + 1 || insertLastID && lenGot >= 2 && !args[1].matches( "[0-9]+" )) ) {
			final String[] args2 = new String[lenGot + 1];
			args2[0] = args[0];
			args2[1] = map.computeLastID();
			System.arraycopy( args, 1, args2, 2, lenGot - 1 );
			args = args2;
		}
		return args;
	}

	/**
	 * @implNote lastID will always be appended: "d" -> "d 1234"
	 *           Insert only wenn explicit allowed: "t foo bar" -> "t 1234 foo bar", "d ." -> "d ."
	 */
	public A_Task_Item( final I_DataStoreManager dsm, final IdMap map, final String[] args, final int min, final Integer max, final boolean insertLastID ) {
		super( dsm, map, A_Task_Item.iArgumentsID( map, args, min, insertLastID ), min, max );
	}

}
