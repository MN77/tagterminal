/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.util.Lib_Comply;


/**
 * @author Michael Nitsche
 * @created 18.03.2022
 */
public abstract class A_Task_Store extends A_Task {

	public A_Task_Store( final I_DataStoreManager dsm, final String[] args ) {
		super( dsm, null, args, 0, 1 );
	}

	@Override
	protected final IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final Integer storeID = this.iArgumentStore( dsm, args );
		return this.exec( dsm, args, storeID );
	}

	protected abstract IdMap exec( I_DataStoreManager dsm, String[] args, Integer storeID );

	private Integer iArgumentStore( final I_DataStoreManager dsm, final String[] args ) {
		return args.length == 0
			? null
			: Lib_Comply.checkStoreID( dsm, args[0] );
	}

}
