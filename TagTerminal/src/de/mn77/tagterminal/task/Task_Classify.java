/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.HashSet;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_ItemID;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 26.02.2022
 */
public class Task_Classify extends A_Task_Item {

	private final boolean remove;


	public Task_Classify( final I_DataStoreManager dsm, final IdMap map, final String[] input, final boolean remove ) {
		super( dsm, map, input, 2, null, true );
		this.remove = remove;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		String resultMId = null;

		final Group2<String[], HashSet<String>> id_tags = this.iSplitIDsTags( args );
		final String[] ids = id_tags.o1;
		final HashSet<String> tags = id_tags.o2;

		if( ids.length == 0 )
			throw new TagTerminalException( "Got no ids to set tags!" );
		if( tags.size() == 0 )
			throw new TagTerminalException( "Missing tags to set!" );

		for( final String id : ids ) {
			final ItemAccess source = new ItemAccess( dsm, idMap, id, true );
			if( !source.getStore().canTags() )
				throw new TagTerminalException( "This store does not support 'tags': " + source.getStore().getStoreID() );

			if( ids.length == 1 )
//				result = source.getID();
				resultMId = id;

			final HashSet<String> tagsToAdd = new HashSet<>();
			final HashSet<String> tagsToDel = new HashSet<>();

			for( String tag : tags ) {
				tag = tag.toLowerCase();

				if( tag.startsWith( "-" ) ) {
					tag = tag.substring( 1 );
					Lib_Comply.checkTag( tag );
					tagsToDel.add( tag );
				}
				else if( tag.startsWith( "+" ) ) {
					tag = tag.substring( 1 );
					Lib_Comply.checkTag( tag );
					tagsToAdd.add( tag );
				}
				else if( this.remove )
					// Allowing to delete invalid tags!
					tagsToDel.add( tag );
				else {
					Lib_Comply.checkTag( tag );
					tagsToAdd.add( tag );
				}
			}
			String[] pageTags = source.getStore().tags().getClasses( source.getID() );
			Err.ifNull( pageTags );

			for( final String tag : tagsToDel )
				if( Lib_Array.contains( pageTags, tag ) )
					source.getStore().tags().declassify( source.getID(), tag );

			for( final String tag : tagsToAdd ) {
				if( !Lib_Array.contains( pageTags, tag ) )
					source.getStore().tags().classify( source.getID(), tag );

				final Collection<String> subTags = dsm.getTagLinks( tag );
				for( final String sub : subTags )
					source.getStore().tags().classify( source.getID(), sub );
			}
			pageTags = source.getStore().tags().getClasses( source.getID() );

			final String message = pageTags.length == 0
				? "No tags set for " + source.getID().getAbsolute()
				: pageTags.length == 1
					? "New tag for " + source.getID().getAbsolute() + ": " + pageTags[0]
					: "New tags for " + source.getID().getAbsolute() + ": " + ConvertArray.toString( ", ", (Object[])pageTags );
			Lib_TTOut.print( message );

			/*
			 * TODO
			 * - prüfen was an Tags bereits gesetzt ist
			 * - Was noch gesetzt werden muss
			 * - Tags evtl. entfernen
			 *
			 * TagLink, TagUnLink
			 *
			 * + - beachten!
			 *
			 * Beispiele:
			 * foo bar bak
			 * foo -bar +bak
			 *
			 */
		}
//		return result;
		return new IdMap( idMap, resultMId );
	}

	private Group2<String[], HashSet<String>> iSplitIDsTags( final String[] args ) {
		final int argsLen = args.length;
		int tagsOffset = argsLen; // behind the last item!

		for( int i = 0; i < args.length; i++ )
			if( !Lib_ItemID.isItemID( args[i] ) ) {
				tagsOffset = i;
				break;
			}

		final String[] ids = new String[tagsOffset];
		if( tagsOffset > 0 )
			System.arraycopy( args, 0, ids, 0, tagsOffset );

		final int tagAmount = argsLen - tagsOffset;
		final String[] tags = new String[tagAmount];
		if( tagAmount > 0 )
			System.arraycopy( args, tagsOffset, tags, 0, tagAmount );

		final String tagsString = ConvertArray.toString( " ", tags );
		final HashSet<String> tagSet = ConvertString.toItemSet( new char[]{ ' ', ',' }, tagsString );

		return new Group2<>( ids, tagSet );
	}

}
