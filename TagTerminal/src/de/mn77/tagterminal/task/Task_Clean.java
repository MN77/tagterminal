/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.base.sys.MInput;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 11.03.2022
 */
public class Task_Clean extends A_Task_Store {

	public Task_Clean( final I_DataStoreManager dsm, final String[] input ) {
		super( dsm, input );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final String[] args, final Integer storeID ) {
		// --- Preparations

		if( storeID != null ) {
			final I_Store store = dsm.getDataStoreMap().get( storeID );
			Lib_Task.checkTrash( store );
			Lib_Task.checkWrite( store );
			Lib_TTOut.print( "Do you really want to empty the trash of store " + storeID + "? (y/N)" );
		}
		else
			Lib_TTOut.print( "Do you really want to empty the trash of all stores? (y/N)" );
		Lib_TTOut.echoIndent();
		final String s = MInput.readStringDefault( "n" );
		if( !s.toLowerCase().equals( "y" ) )
			throw new TagTerminalException( "Action aborted!" );

		// --- Delete

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( storeID != null && store.getKey() != storeID || !store.getValue().canTrash() )
				continue;

			final SearchResult result = store.getValue().items().trash();

			for( final SearchResultItem item : result ) {
				// Delete one page
				store.getValue().delete( item.getID() );
				Lib_TTOut.print( "Page deleted: " + item.getID().getAbsolute() );
			}
		}
		return new IdMap();
	}

}
