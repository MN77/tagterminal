/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.base.error.Err_FileSys;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 07.03.2022
 */
public class Task_Clone extends A_Task_Item {

	public Task_Clone( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 1, 1, false );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final ItemAccess source = new ItemAccess( dsm, idMap, args[0], false );
		final I_ItemData s = source.getStore().items().getItem( source.getID(), false );
		final I_ItemData sNew = dsm.tool().clone( s );

		final I_Store target = dsm.getWriteStore();
		Lib_Task.checkWrite( target );

		ItemID newID;

		try {
			newID = target.items().add( sNew );
		}
		catch( Err_FileSys e ) {
			throw new TagTerminalException(e.getMessage());
		}

		if( newID != null )
			Lib_TTOut.print( "Page cloned to ID: " + newID.getAbsolute() );
		else
			Lib_TTOut.print( "Page could not be cloned!" );

		return new IdMap( newID, sNew.getTitle(), sNew.getTags() );
	}

}
