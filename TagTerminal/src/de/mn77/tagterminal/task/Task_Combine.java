/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MInput;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 07.03.2022
 */
public class Task_Combine extends A_Task {

	public Task_Combine( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 2, 2 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final ItemAccess source1 = new ItemAccess( dsm, idMap, args[0], false );
		final ItemAccess source2 = new ItemAccess( dsm, idMap, args[1], false );
		final I_ItemData s1 = source1.getStore().items().getItem( source1.getID(), false );
		final I_ItemData s2 = source2.getStore().items().getItem( source2.getID(), false );

		final I_ItemData sNew = dsm.tool().combine( s1, s2 );

		final I_Store target = dsm.getWriteStore();
		Lib_Task.checkWrite( target );

		ItemID newID;

		try {
			newID = target.items().add( sNew );
		}
		catch( Err_FileSys e ) {
			throw new TagTerminalException(e.getMessage());
		}

		if( newID != null )
			Lib_TTOut.print( "Pages combined to ID: " + newID.getAbsolute() );
		else
			Lib_TTOut.print( "Pages could not be combined!" );

		// Delete page(s)
		if( !source1.getID().getAbsolute().equals( source2.getID().getAbsolute() ) ) // Don't delete if a page is combined with himself.
			this.iDelete( source1, source2 );

		return new IdMap( newID, sNew.getTitle(), sNew.getTags() );
	}

	private void iDelete( final ItemAccess source1, final ItemAccess source2 ) {
		final boolean write1 = source1.getStore().canWrite();
		final boolean write2 = source2.getStore().canWrite();

		if( write1 || write2 ) {
			if( write1 && write2 )
				Lib_TTOut.print( "Delete the original pages? (y/N)" );
			else if( write1 )
				Lib_TTOut.print( "Delete the original page " + source1.getID().getAbsolute() + "? (y/N)" );
			else if( write2 )
				Lib_TTOut.print( "Delete the original page " + source2.getID().getAbsolute() + "? (y/N)" );

			Lib_TTOut.echoIndent();
			final String s = MInput.readStringDefault( "n" );

			if( s.toLowerCase().equals( "y" ) ) {

				if( write1 ) {
					source1.getStore().delete( source1.getID() );
					Lib_TTOut.print( "Page " + source1.getID().getAbsolute() + " deleted." );
				}

				if( write2 ) {
					source2.getStore().delete( source2.getID() );
					Lib_TTOut.print( "Page " + source2.getID().getAbsolute() + " deleted." );
				}
			}
		}
	}

}
