/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MInput;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.ARG_TYPE;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 17.02.2022
 */
public class Task_CopyMove extends A_Task_Item {

	private final boolean delete;


	public Task_CopyMove( final I_DataStoreManager dsm, final IdMap last, final String[] input, final boolean delete ) {
		super( dsm, last, input, 2, null, false );
		this.delete = delete;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		if( dsm.isSingleStore() )
			throw new TagTerminalException( "There is only one store, so nothing can be moved or copied!" );

		ItemID result = null;
		String resultTitle = null;
		String[] resultTags = null;
		int done = 0;

		final int argsLen = args.length;
		final String[] sources = U_StringArray.cutLeft( args, argsLen - 1 );
		final String dest = args[argsLen - 1];

		final Integer destStoreID = Lib_Comply.checkStoreID( dsm, dest );
		final I_Store destStore = dsm.getDataStoreMap().get( destStoreID );

		if(!destStore.isEnabled())
			throw new TagTerminalException( "Target store is disabled!" );

		for( final String source : sources ) {
			final ARG_TYPE type = Lib_Task.identify( source );

			if( type == ARG_TYPE.STORE ) {
				// Move all from STORE to STORE // but ignore trash!

				final Integer sourceStoreID = Lib_Comply.checkStoreID( dsm, source );
				final I_Store sourceStore = dsm.getDataStoreMap().get( sourceStoreID );
				this.iCheckStores( sourceStore, destStore, this.delete, true );

				final Collection<I_ItemData> toCopy = sourceStore.items().getAllItems();

				for( final I_ItemData page : toCopy ) {
					if( !page.getTrash() ) {
						this.iCopyOne( page, destStore, this.delete );
						done++;
					}

//					if( this.delete )
//						sourceStore.delete( page.getID() ); // TODO einzeln löschen: vielleicht so
				}

				if( this.delete )
					sourceStore.deleteAll(); // TODO einzeln löschen!!!
			}
			else if( type == ARG_TYPE.ITEM ) {
				// Move one page from STORE to STORE

				final ItemAccess page = new ItemAccess( dsm, idMap, source, this.delete );
				this.iCheckStores( page.getStore(), destStore, this.delete, false ); // Source wird doppelt geprüft

				final I_ItemData s = page.getStore().items().getItem( page.getID(), false );

				final ItemID newID = this.iCopyOne( s, destStore, this.delete );
				if( this.delete )
					page.getStore().delete( page.getID() );

				done++;

				if( sources.length == 1 ) {
//				result = source.getID();
					result = newID;
					resultTitle = s.getTitle();
					resultTags = s.getTags();
				}
			}
			else if( type == ARG_TYPE.TAG ) { // Alle pages with this tag
				final String tag = source.toLowerCase();
				Lib_Comply.checkTag( tag );
				final String[] search = { tag };

				for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
					if( destStoreID == store.getKey() || !store.getValue().canTags() )
						continue;

					final SearchResult searchResult = store.getValue().items().searchTags( search, false );

					for( final SearchResultItem page : searchResult ) {
						final I_ItemData dataPage = store.getValue().items().getItem( page.getID(), false );

						if( !dataPage.getTrash() ) {
							this.iCopyOne( dataPage, destStore, this.delete );
							done++;
						}
						if( this.delete )
							store.getValue().delete( page.getID() );
					}
				}
			}
		}

		if( done > 1 )
			Lib_TTOut.print( done + " pages " + (this.delete ? "moved" : "copied") );

		return new IdMap( result, resultTitle, resultTags );
	}

	private void iCheckStores( final I_Store sourceStore, final I_Store destStore, final boolean delete, final boolean all ) {
		if( sourceStore == destStore )
			throw new TagTerminalException( "Source and destination are the same!" );

		Lib_Task.checkWrite( destStore );
		if( delete )
			Lib_Task.checkWrite( sourceStore );

		final ArrayList<String> fail = new ArrayList<>();
		if( !destStore.canHits() )
			fail.add( "Hits" );
		if( !destStore.canTags() )
			fail.add( "Tags" );
		if( all && !destStore.canTrash() )
			fail.add( "Trash" );

		if( fail.size() > 0 ) {
			Lib_TTOut.print( "Destination getStore() does not support the following things: " + ConvertSequence.toString( ", ", fail ) );
			Lib_TTOut.print( "So, some data will be lost!" );
			Lib_TTOut.print( "Is this okay for you? (y/N)" );
			Lib_TTOut.echoIndent();
			final char c = MInput.readCharDefault( 'N' );

			if( c != 'y' )
				throw new TagTerminalException( "Action aborted!" );
		}
	}

	private ItemID iCopyOne( final I_ItemData page, final I_Store destStore, final boolean delete ) {
		ItemID newID;

		try {
			newID = destStore.items().add( page );
		}
		catch( Err_FileSys e ) {
			throw new TagTerminalException(e.getMessage());
		}

		final StringBuilder sb = new StringBuilder();
		sb.append( "Page " );
		sb.append( page.getID().getAbsolute() );
		sb.append( " " );
		sb.append( delete ? "moved" : "copied" );
		sb.append( " from store " );
		sb.append( page.getID().storeID );
		sb.append( " to store " );
		sb.append( destStore.getStoreID() );
		sb.append( ". New ID is: " );
		sb.append( newID.getAbsolute() );
		Lib_TTOut.print( sb.toString() );

		return newID;
	}

}
