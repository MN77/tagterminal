/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.base.sys.MInput;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.ARG_TYPE;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 15.02.2022
 */
public class Task_Delete extends A_Task_Item {

	public Task_Delete( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 1, null, false );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
//		PageID result = null;
		String resultMId = null;

		for( String id : args ) {
			id = id.trim();
			final ARG_TYPE type = Lib_Task.identify( id );

			if( type == ARG_TYPE.STORE ) {
				// Delete all items from one getStore()

				final Integer storeID = Lib_Comply.checkStoreID( dsm, id );
				final I_Store store = dsm.getDataStoreMap().get( storeID );
				Lib_Task.checkWrite( store );

				Lib_TTOut.print( "Do you really want to DELETE ALL PAGES from getStore() " + storeID + ", including trash?!" );
				Lib_TTOut.print( "If this is what you want, please type uppercase 'yes':" );

				Lib_TTOut.echoIndent();
				final String s = MInput.readStringDefault( "no" ).trim();
				if( !s.equals( "YES" ) )
					throw new TagTerminalException( "Action aborted!" );

				store.deleteAll();
				Lib_TTOut.print( "All pages from getStore() " + storeID + " deleted!" );
			}
			else if( type == ARG_TYPE.ITEM ) {
				// Delete one page

				final ItemAccess source = new ItemAccess( dsm, idMap, id, true );

				if( args.length == 1 )
//					result = source.getID();
					resultMId = id;

				final boolean canTrash = source.getStore().canTrash();

				// hard = really delete, soft = to trash
				boolean hardDelete = canTrash
					? source.getStore().items().isInTrash( source.getID() )
					: true;

				if( !hardDelete && !canTrash ) {
					Lib_TTOut.print( "This getStore() has no trash! Delete " + source.getID().getAbsolute() + " directly? (y/N)" );
					final String s = MInput.readStringDefault( "n" );
					if( !s.toLowerCase().equals( "y" ) )
						throw new TagTerminalException( "Action aborted!" );
					else
						hardDelete = true;
				}

				if( hardDelete ) {
					source.getStore().delete( source.getID() );
					Lib_TTOut.print( "Page deleted: " + source.getID().getAbsolute() );
				}
				else {
					source.getStore().items().moveToTrash( source.getID() );
					Lib_TTOut.print( "Page moved to trash: " + source.getID().getAbsolute() );
				}
			}
			else if( type == ARG_TYPE.TAG ) {
				// Delete all items with one tag

				final String tag = id.toLowerCase();
				Lib_Comply.checkTag( tag );
				final String[] search = { tag };

				Lib_TTOut.print( "Do you really want to DELETE ALL PAGES with tag '" + tag + "'? (y/N)" );
				Lib_TTOut.echoIndent();
				final String s = MInput.readStringDefault( "no" ).trim();

				if( !s.startsWith( "y" ) )
					throw new TagTerminalException( "Action aborted!" );

				for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
					if( !store.getValue().canTags() )
						continue;

					final SearchResult searchResult = store.getValue().items().searchTags( search, false );

					for( final SearchResultItem page : searchResult ) {
						final I_ItemData dataPage = store.getValue().items().getItem( page.getID(), false );

						if( !dataPage.getTrash() ) {
							store.getValue().items().moveToTrash( page.getID() );
							Lib_TTOut.print( "Page " + page.getID().getAbsolute() + " moved to trash." );
						}
					}
				}
			}
		}
		return new IdMap( idMap, resultMId );
	}

}
