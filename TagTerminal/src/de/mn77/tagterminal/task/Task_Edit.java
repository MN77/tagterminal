/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.io.File;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.SysDir;
import de.mn77.tagterminal.data.EDIT_RESULT;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.OUTPUT_FORMAT;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Task_Edit extends A_Task_Item {

	private final boolean gui;


	public Task_Edit( final I_DataStoreManager dsm, final IdMap map, final String[] input, final boolean gui ) {
		super( dsm, map, input, 1, 1, false );
		this.gui = gui;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final ItemAccess source = new ItemAccess( dsm, idMap, args[0], true );
		final I_ItemData s = source.getStore().items().getItem( source.getID(), false );

		final Group2<EDIT_RESULT, I_ItemData> editResult = dsm.tool().edit( s, this.gui );

		switch( editResult.o1 ) {
			case SAME:
				Lib_TTOut.print( "Nothing changed." );
//				return source.getID();
//				return idMap.copy(args[0]);
				break;
			case DELETED:
				source.getStore().delete( source.getID() );
				Lib_TTOut.print( "Page deleted: " + source.getID().getAbsolute() );
				break;
			case CHANGED:
				try {
					source.getStore().items().update( editResult.o2 );
					Lib_TTOut.print( "Page updated: " + source.getID().getAbsolute() );
				}
				catch( Exception e ) {
					StringBuilder sb = new StringBuilder();
					sb.append( dsm.tool().getPrefix() );
					sb.append( "RESCUE_" );
					sb.append( source.getID().getAbsolute() );

					try {
//						final File tmpFile = File.createTempFile( sb.toString(), ".wiki" );
						final File tmpFile = SysDir.home().fileNextAbsent( sb.toString(), "wiki" ).getFile();
						Lib_TextFile.set( tmpFile, dsm.tool().convert( editResult.o2, OUTPUT_FORMAT.WIKI ) );
						Lib_TTOut.print( "STORE ERROR! Changes rescued to: "+tmpFile.getAbsolutePath() );
					}
					catch( Exception e1 ) {
						Err.show(e1);
					}
					throw e;
				}

				break;
		}
//		return sEdited.getID();
//		return idMap;
		return new IdMap( idMap, args[0] );
	}

}
