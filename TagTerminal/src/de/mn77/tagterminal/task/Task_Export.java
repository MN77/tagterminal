/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.Map.Entry;

import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.MDir;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.ARG_TYPE;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 08.03.2022
 */
public class Task_Export extends A_Task_Item {

	public Task_Export( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 1, null, false );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		String destDir = null;

		String[] ids = args;

		if( ids.length >= 2 && !ids[ids.length - 1].matches( "[1-9]+" ) ) {
			destDir = ids[ids.length - 1];
			ids = U_StringArray.cutLeft( ids, ids.length - 1 );
		}
		MDir dir = null;

		try {
			dir = destDir != null
				? new MDir( destDir, true ) // create if missing
				: new MDir( Sys.getCurrentDir() );
		}
		catch( final Err_FileSys e ) {
			throw new TagTerminalException( e.getMessage() );
		}
		String lastMId = null;

		for( String id : ids ) {
			id = id.trim();
			final ARG_TYPE type = Lib_Task.identify( id );

			if( type == ARG_TYPE.STORE ) { // Complete getStore()
				final Integer storeID = Lib_Comply.checkStoreID( dsm, id );
				final I_Store store = dsm.getDataStoreMap().get( storeID );

				final Collection<I_ItemData> allPages = store.items().getAllItems();
				for( final I_ItemData page : allPages )
					dsm.tool().export( dir, page );
			}
			else if( type == ARG_TYPE.ITEM ) { // One single page
				final ItemAccess source = new ItemAccess( dsm, idMap, id, false );
				final I_ItemData page = source.getStore().items().getItem( source.getID(), true ); // Hit zählen? true?
				dsm.tool().export( dir, page );

				if( ids.length == 1 )
//					result = source.getID();
					lastMId = id;
			}
			else if( type == ARG_TYPE.TAG ) { // Alle pages with this tag
				final String tag = id.toLowerCase();
				Lib_Comply.checkTag( tag );
				final String[] search = { tag };

				for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
					if( !store.getValue().canTags() )
						continue;

					final SearchResult searchResult = store.getValue().items().searchTags( search, false );

					for( final SearchResultItem page : searchResult ) {
						final I_ItemData dataPage = store.getValue().items().getItem( page.getID(), false );
						dsm.tool().export( dir, dataPage );
					}
				}
			}
		}
		return new IdMap( idMap, lastMId );// result;
	}

}
