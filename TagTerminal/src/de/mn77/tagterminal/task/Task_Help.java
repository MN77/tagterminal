/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.ArrayList;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Task_Help extends A_Task {

	private int           outLines = 0;
	private final String  search;
	private final boolean slim;


	public Task_Help( final I_DataStoreManager dsm, final IdMap map, final String[] input, final boolean slim ) {
		super( dsm, map, input, 0, slim ? 1 : 0 );

		this.search = Lib_Task.emptyInput( input ) || input.length == 1
			? null
			: input[1];

		this.slim = this.search != null
			? false
			: slim;
	}

	/**
	 * @implNote
	 *           <> = required
	 *           [] = optional
	 */
	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		this.outLines = 0;

		this.iHeadline( "Search" );
		this.iLine( "a,all", "[STORE]", "List all pages. If an argument is given, show only the pages of this store.", "a", "a 2" );
		this.iLine( "biggest", "[STORE] <Amount>", "Search the biggest pages.", "biggest 10", "biggest 2 10" );
		this.iLine( "f,find", "[STORE] <...>", "Find titles which matching all keywords.", "f foo bar", "f 2 foo" );
		this.iLine( "F,Find", "[STORE] <...>", "Find titles which matching any keywords.", "F foo bar", "F 2 foo" );
		this.iLine( "last", "[STORE] <Amount>", "Search the last changed pages.", "late 10", "late 8 10" );
		this.iLine( "newest", "[STORE] <Amount>", "Search the newest created pages.", "newest 10", "newest 8 10" );
		this.iLine( "s,search", "[STORE] <...>", "Search with matching all keywords.", "s foo bar", "s 2 foo" );
		this.iLine( "S,Search", "[STORE] <...>", "Search with matching any keywords.", "S foo bar", "S 2 foo" );
		this.iLine( "smallest", "[STORE] <Amount>", "Search the smallest pages.", "smallest 10", "smallest 2 10" );
		this.iLine( "top", "[STORE] <Amount>", "Search the most used pages.", "top 10", "top 8 10" );
//		this.iLine("r,sreg,regex", "<...>", "Search by regular expression", "r .*foo.*"); // TODO R für case-sensitive ?!? TODO [STORE]
		this.iLine( "x,trash", "[STORE]", "Show trash. If an argument is given, show only the trash of this store.", "x", "x 2" );

		this.iHeadline( "Tags" );
		this.iLine( "list", "", "List all known and used tags.", "list" );
		this.iLine( "tree", "[STORE]", "Show a tree with all tags.", "tree", "tree 2" );
		this.iLine( "c,class", "<ID...> <[+|-]TAG ...>", // TODO l für link, oder g für group und c für crypt ?!?
			"Classify page with these tags. Tags with leading '-' will be removed, tags with optional '+' will be added.", //  The default is that the tags are removed or set so that the page is linked exactly with the specified tags.
			"c 8003 8009 foo bar" );
		this.iLine( "C,Class", "<ID> [[+|-]TAG ...]",
			"Remove tags for this page. Tags with optional leading '-' will be removed, tags with leading '+' will be added.", //  The default is that the tags are removed or set so that the page is linked exactly with the specified tags.
			"C 8003 foo bar" );
		this.iLine( "l,link", "<TAG> <TAG>", "Link first tag to the second tag.", "link foo bar" );
		this.iLine( "L,unlink", "<TAG> <TAG>", "Remove the link from first tag to the second tag.", "unlink foo bar" );
		this.iLine( "links", "", "Show all linked tags.", "links" );
		this.iLine( "t,tag", "[STORE] <TAG...>", "Search pages which are linked to all these tags.", "t foo bar" );
		this.iLine( "T,Tag", "[STORE] <TAG...>", "Search pages which are linked to one of these tags.", "T foo bar" );
		this.iLine( "merge", "[STORE] <TAG> <TAG>", "Rename/Merge tag1 to/with tag2.", "merge foo bar", "merge 2 foo bar" );

		this.iHeadline( "Page info" );
		this.iLine( "<ID>,o,open", "<ID>", "Open (or show) page", "8003", "open 8003" );
		this.iLine( "show", "<ID>", "Show page", "8003", "show 8003" );
		this.iLine( "changed", "<ID>", "Show the last change time", "changed 8003" );
		this.iLine( "created", "<ID>", "Show the create time", "created 8003" );
		this.iLine( "describe", "<ID>", "Describe page (this is for debugging)", "describe 8003" );
		this.iLine( "e,edit", "<ID>", "Edit page", "e 8003" );
		this.iLine( "E,Edit", "<ID>", "Edit page with GUI-Editor", "E 8003" );
		this.iLine( "hits", "<ID>", "Show the amount of hits for this page", "hits 8003" );
		this.iLine( "meta,info", "<ID>", "Show the meta information of a page", "info 8003" );
		this.iLine( "tags", "<ID>", "Show the linked tags of this page", "tags 8003" );
		this.iLine( "title", "<ID>", "Show the title of the page", "title 8003" );
		this.iLine( "rename", "<ID>", "Edit page title", "rename 8003" );
		this.iLine( "Rename", "<ID>", "Edit page title with GUI-Editor", "Rename 8003" );

		this.iHeadline( "Page admin" );
		this.iLine( "clone", "<ID>", "Duplicate a page to the default write store.", "clone 8003" ); // TODO Evtl.: clone 8003 8
		this.iLine( "combine", "<ID> <ID>", "Combine two pages.", "combine 8003 9014" ); // TODO Combine also 3,4,5,... pages
		this.iLine( "cp,copy", "<ID...|STORE...|TAG...> <STORE>", "Copy one or all pages to another store", "copy 8003 2", "copy 8 2" );
		this.iLine( "d,del", "<ID...|STORE...>", "Delete single pages or all pages from a store. Single pages will be moved to trash first. A second 'delete' will delete it finally.",
			"d 8003" );
		this.iLine( "diff", "<ID> <ID>", "Show the difference between to pages.", "diff 8003 9014" );
		this.iLine( "Diff", "<ID> <ID>", "Show the difference between to pages with GUI.", "Diff 8003 9014" );
		this.iLine( "export", "<ID...|STORE...|TAG...> [Directory]", "Export pages or a complete store to the current or a selected directory", "export 8003", "export 4 /tmp/store4",
			"export 8003 8005 9" );
		this.iLine( "import", "[STORE] <Filepattern>", "Import one or more files to default or given store. Wildcards can be used.", "import foo.txt", "import *.txt", "import 3 A*.txt" );
		this.iLine( "mv,move", "<ID...|STORE...|TAG...> <STORE>", "Move one or all pages to another store", "move 8003 2", "move 8 2" );
		this.iLine( "n,new", "[STORE]", "Create new page. If an argument is given, the new page will be created in this store.", "n", "n 2" );
		this.iLine( "N,New", "[STORE]", "Create new page with GUI-Editor.", "N", "N 2" );
		this.iLine( "u,undel", "<ID ...>", "Undelete page(s) from trash.", "u 8003" );

		this.iHeadline( "General" );
		this.iLine( "-", "", "Clear mapping", "-" );
		this.iLine( "h,help", "[...]", "Show command overview or command help page", "h", "help new" );
		this.iLine( "man,manual", "", "Show full manual", "man" );
		this.iLine( "?,k,keys", "", "Show the keys from last search result", "?", "k" );
		this.iLine( "m,map", "", "Show current mapping", "map" );	// mapping
		this.iLine( "q,quit,exit", "", "Exit command line interface.", "q", "exit" );
		this.iLine( "stores", "", "Print a table with all configured stores.", "stores" );
		this.iLine( "on", "[STORE]", "Enable a disabled store.", "on 2" );
		this.iLine( "off", "[STORE]", "Disable a store.", "off 2" );
		this.iLine( "status", "", "Print a statistic with the amount of items and tags.", "status" );
		this.iLine( "v,version", "", "Show version.", "version" );
		this.iLine( "X,clean", "[STORE]", "Empty the trash of all stores or a single one.", "clean", "X 2" );

		if( this.search != null && this.outLines == 0 )
			Lib_TTOut.print( "Unknown command: " + this.search );

		return idMap;
	}

	private void iHeadline( final String title ) {
		if( this.search != null )
			return;

		Lib_TTOut.print( title.toUpperCase() );
	}

	private void iLine( final String keys, final String options, final String info, final String... examples ) {
		final String space = "      ";
		final String[] keyArr = keys.split( "," );
		if( this.search != null && !Lib_Array.contains( keyArr, this.search ) )
			return;

		Lib_TTOut.print( space + ConvertArray.toString( ", ", (Object[])keyArr ) + "  " + options );

		if( !this.slim ) {
			final Iterable<String> lines = this.iLines( space + space, info );

			for( final String line : lines )
				Lib_TTOut.print( space + space + line );

			for( final String exp : examples )
				Lib_TTOut.print( space + space + "-->    " + exp );

			Lib_TTOut.print();
		}
		this.outLines++;
	}

	private Iterable<String> iLines( final String indent, final String text ) {
		final ArrayList<String> result = new ArrayList<>();
		final int indentLen = indent.length();
		int start = 0;
		int lastSpace = 0;

		for( int i = 0; i < text.length(); i++ ) {
			final char c = text.charAt( i );

			if( c == ' ' ) {
				lastSpace = i;
				continue;
			}

			// End of text
			if( i == text.length() - 1 ) {
				result.add( text.substring( start ) );
				return result;
			}

			// End of line
			if( indentLen + i - start == 78 ) {
				result.add( text.substring( start, lastSpace ) );
				start = lastSpace + 1;
				i = lastSpace + 1;
			}
		}
		return result;
	}

}
