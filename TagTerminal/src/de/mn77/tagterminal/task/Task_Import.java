/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;

import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.Lib_FileSys;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 26.02.2022
 */
public class Task_Import extends A_Task {

	/**
	 * 1 or more arguments needed!
	 * First argument can be a getStore()-ID
	 */
	public Task_Import( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 1, null );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		Integer storeID = null;
		String[] files = null;

		if( args.length >= 2 && args[0].length() == 1 && args[0].matches( "[1-9]" ) ) {
			storeID = Lib_Comply.checkStoreID( dsm, args[0] );
			files = U_StringArray.cutFrom( args, 1 );
		}
		else // args.len = 1
			files = args;
		final I_Store store = storeID == null
			? dsm.getWriteStore()
			: dsm.getDataStoreMap().get( storeID );

		Lib_Task.checkWrite( store );

		ItemID resultID = null;
		String resultTitle = null;
		String[] resultTags = null;
		int index = 0;

		try {

			for( String filePattern : files ) {
				File base = null;

				if( filePattern.startsWith( "/" ) ) {
					final String path = CutString.tillLast( filePattern, '/', false );
					base = new File( path );
					filePattern = CutString.cut( filePattern, path.length(), false );
				}
				else
					base = new File( Sys.getCurrentDir() );
				final DirectoryStream<Path> paths = Lib_FileSys.searchFiles( base, filePattern );

				for( final Path path : paths ) {
					final File curFile = path.toFile();
					final I_ItemData sNew = dsm.tool().importItem( curFile );
					final ItemID newID = store.items().add( sNew );
					if( newID != null )
						Lib_TTOut.print( "New page saved with ID: " + newID.getAbsolute() );
					else
						Lib_TTOut.print( "The new page could not be created!" );

					resultID = index == 0
						? newID
						: null;
					resultTitle = index == 0
						? sNew.getTitle()
						: null;
					resultTags = index == 0
						? sNew.getTags()
						: null;
					index++;
				}
			}
		}
		catch( final Exception e ) {
			throw Err.exit( e );
		}
		return new IdMap( resultID, resultTitle, resultTags );
	}

}
