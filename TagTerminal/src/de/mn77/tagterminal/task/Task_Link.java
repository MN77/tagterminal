/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.Map.Entry;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 16.09.2022
 */
public class Task_Link extends A_Task {

	public Task_Link( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 2, 2 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final String tagBase = args[0];
		final String tagTarget = args[1];

		Lib_Comply.checkTag( tagBase );
		Lib_Comply.checkTag( tagTarget );

		dsm.linkTags( tagBase, tagTarget );

		final Collection<String> tagsToLink = dsm.getTagLinks( tagTarget );

		// Search pages
		final String[] search = { tagBase };
//		final ArrayList<SearchResultItem> found = new ArrayList<>();

		int counter = 0;

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( !store.getValue().canTags() )
				continue;

			final SearchResult result = store.getValue().items().searchTags( search, true );
//			found.addAll(result);

			if( result != null )
				for( final SearchResultItem item : result ) {
					counter++;

					store.getValue().tags().classify( item.getID(), tagTarget );

					for( final String tag : tagsToLink )
						store.getValue().tags().classify( item.getID(), tag );
				}
		}
		Lib_TTOut.print( "Linked: " + tagBase + " --> " + tagTarget + ", " + ConvertSequence.toString( ", ", tagsToLink ) );
		Lib_TTOut.print( "Pages updated: " + counter );

		return idMap;
	}

}
