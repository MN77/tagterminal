/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 03.09.2024
 */
public class Task_ListLinks extends A_Task {

	public Task_ListLinks( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 0, 1 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		I_Table<String> table = new ArrayTable<>(3);

		if(args == null || args.length == 0) {
			I_Table<String> links = dsm.getAllTagLinks( );

			for(int i=0; i<links.size(); i++)
				table.addRow( links.get( 0, i ), "→", links.get(1, i) );
		}
		else {
			Collection<String> links = dsm.getTagLinks( args[0] );

			for(String link : links)
				table.addRow( args[0], "→", link );
		}

		// --- Output ---
		final TableStyler styler = new TableStyler();
		styler.colLineStyleDefault( '2' ); // !
		styler.setIndent( TAG_TERMINAL.INDENT_NUM );
//		styler.rowLineStyles( "=" );
		styler.alignDefault( ALIGN.LEFT );
		styler.alignColumn( 1, ALIGN.CENTER );
		Lib_TTOut.printRaw( styler.compute( table ) );

		return idMap;
	}

}
