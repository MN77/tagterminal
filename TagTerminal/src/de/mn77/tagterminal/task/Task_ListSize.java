/*******************************************************************************
 * Copyright (C) 2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.Map.Entry;

import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Task_ListSize extends A_Task {

	private final boolean small;

	public Task_ListSize( final I_DataStoreManager dsm, final IdMap map, final String[] input, final boolean small ) {
		super( dsm, map, input, 0, 2 );
		this.small = small;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		Integer storeID = null;
		if( args.length == 2 )
			storeID = Lib_Comply.checkStoreID( dsm, args[0] );

		int top = TAG_TERMINAL.LIST_DEFAULT_LENGTH;
		if( args.length >= 1 )
			try {
				top = Integer.parseInt( args[args.length - 1] );
			}
			catch( final NumberFormatException nfe ) {
				throw new TagTerminalException( "Given argument is not a number: " + args[args.length - 1] );
			}

		final TypeTable2<ItemMetaData, Long> sum = new TypeTable2<>( ItemMetaData.class, Long.class );

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( storeID != null && store.getKey() != storeID )
				continue;

			final Collection<ItemMetaData> result = store.getValue().items().requestMetaData( ITEM_META_TYPE.SIZE, this.small, top );

			if(result != null) // Maybe null, if store has no meta data
				for( final ItemMetaData meta : result )
					sum.add( meta, meta.getSize() );
		}

		sum.sort( this.small ? 2 : -2 );

		return Lib_TTOut.showResult( sum.getColumn0(), top, ITEM_META_TYPE.SIZE );
	}

}
