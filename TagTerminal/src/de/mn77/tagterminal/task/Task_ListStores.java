/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 05.03.2022
 *
 *          TODO: Max. width 80 chars?
 */
public class Task_ListStores extends A_Task {

	public Task_ListStores( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 0, 0 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final ArrayTable<String> table = new ArrayTable<>( 9 );
		table.addRow( "ID", "On", "Type", "Info", "Write", "Trash", "Tag", "Hit", "Default" );

		int defaultStoreID = -1;
		final I_Store defaultStore = dsm.getDefaultStore();
		if( defaultStore != null )
			defaultStoreID = defaultStore.getStoreID();

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMapFull().entrySet() ) {
			final String[] row = new String[table.width()];

			row[0] = "" + store.getKey();
			row[1] = store.getValue().isEnabled() ? "X" : "-";
			row[2] = store.getValue().getType();
			row[3] = store.getValue().getInfo();
			row[4] = store.getValue().canWrite() ? "X" : "-";
			row[5] = store.getValue().canTrash() ? "X" : "-";
			row[6] = store.getValue().canTags() ? "X" : "-";
			row[7] = store.getValue().canHits() ? "X" : "-";
			row[8] = store.getValue().getStoreID() == defaultStoreID ? "X" : "-";

			table.add( row );
		}
		final TableStyler styler = new TableStyler();
		styler.colLineStyleDefault( '|' );
		styler.setIndent( TAG_TERMINAL.INDENT_NUM );
		styler.rowLineStyles( "=" );
		styler.alignDefault( ALIGN.CENTER );
		styler.alignColumn( 0, ALIGN.LEFT );
		styler.alignColumn( 2, ALIGN.LEFT );
		styler.alignColumn( 3, ALIGN.LEFT );
		Lib_TTOut.printRaw( styler.compute( table ) );

		return idMap;
	}

}
