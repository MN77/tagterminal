/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.struct.SimpleMap;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 28.02.2021
 */
public class Task_ListTags extends A_Task {

	public Task_ListTags( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 0, 0 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		// --- Collect ---
		final Map<String, Integer> sum = new SimpleMap<>();

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() )
			if( store.getValue().canTags() )
				for( final Entry<String, Integer> e : store.getValue().tags().getTagStats().entrySet() ) {
					final String key = e.getKey();

					if( sum.containsKey( key ) )
						sum.put( key, sum.get( key ) + e.getValue() );
					else
						sum.put( key, e.getValue() );
				}

		// --- Create Table ---
		final ArrayTable<String> table = new ArrayTable<>( 3 );

		for( final Entry<String, Integer> e : sum.entrySet() ) {
			String linked = "";

			final Collection<String> l = dsm.getTagLinks( e.getKey() );

			if( l.size() > 0 ) {
				final StringBuilder sb = new StringBuilder();
//				sb.append("--> ");

				boolean comma = false;

				for( final String s : l ) {
					if( comma )
						sb.append( ", " );
					sb.append( s );
					comma = true;
				}

				linked = sb.toString();
			}

			table.addRow( e.getKey(), "" + e.getValue(), linked );
		}

		table.sort( 1 );
		table.insertRow( 0, "Tag", "Uses", "Linked" );

		// --- Output ---
		final TableStyler styler = new TableStyler();
		styler.colLineStyleDefault( '!' ); // !
		styler.setIndent( TAG_TERMINAL.INDENT_NUM );
		styler.rowLineStyles( "=" );
		styler.alignDefault( ALIGN.LEFT );
		styler.alignColumn( 1, ALIGN.RIGHT );
		Lib_TTOut.printRaw( styler.compute( table ) );

		return idMap;
//		return new IdMap();
	}

//	private void iTreeOutput(PTreeNode<String,Integer> n, int indent) {
//		for(PTreeNode<String,Integer> node : n) {
//			String left = Lib_String.sequence("  ", indent);
//			MOut.print(left + node.getName());
//			this.iTreeOutput(node, indent+1);
//		}
//	}

}
