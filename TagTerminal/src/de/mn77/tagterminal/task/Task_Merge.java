/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.Map.Entry;

import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 26.11.2024
 */
public class Task_Merge extends A_Task_Item {

	public Task_Merge( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 2, 2, false );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, String[] args ) {
		Integer storeID = null;

		if( args.length >= 1 && args[0].matches( "[1-9]" ) ) {
			storeID = Lib_Comply.checkStoreID( dsm, args[0] );
			args = U_StringArray.cutFrom( args, 1 );
		}
		if( args.length != 2 )
			throw new TagTerminalException( "Invalid amount of tags! Need 2 (tag,target), but got " + args.length );

		for( int i = 0; i < args.length; i++ )
			args[i] = args[i].toLowerCase(); // Invalid tags are allowed here!

		final String[] search = { args[0] };
		Lib_Comply.checkTag( args[1] );

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( !store.getValue().canTags() || storeID != null && store.getKey() != storeID )
				continue;

			final SearchResult result = store.getValue().items().searchTags( search, false );

			for( final SearchResultItem sri : result ) {
				this.iClassify( dsm, sri, args[0], true );
				this.iClassify( dsm, sri, args[1], false );

				final StringBuilder sb = new StringBuilder();
				sb.append( sri.getID().getAbsolute() );
				sb.append( "  " );
				sb.append( args[0] );
				sb.append( " -> " );
				sb.append( args[1] );
				Lib_TTOut.print( sb.toString() );
			}
		}

		return new IdMap();
	}

	private void iClassify( final I_DataStoreManager dsm, final SearchResultItem sri, final String tag, final boolean remove ) {
		final ItemAccess item = new ItemAccess( dsm, sri.getID(), sri.getID().getAbsolute(), true );

		if( remove )
			// Allowing to delete invalid tags!
			item.getStore().tags().declassify( item.getID(), tag );
		else {
			if( !Lib_Array.contains( sri.getTags(), tag ) )
				item.getStore().tags().classify( item.getID(), tag );

			final Collection<String> subTags = dsm.getTagLinks( tag );
			for( final String sub : subTags )
				item.getStore().tags().classify( item.getID(), sub );
		}
	}

}
