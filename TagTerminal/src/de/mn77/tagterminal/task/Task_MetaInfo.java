/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.util.Lib_String;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 11.03.2022
 */
public class Task_MetaInfo extends A_Task_Item {

	private final META_TYPE filter;


	public Task_MetaInfo( final I_DataStoreManager dsm, final IdMap map, final String[] input, final META_TYPE filter ) {
		super( dsm, map, input, 1, 1, false );
		this.filter = filter;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final ItemAccess source = new ItemAccess( dsm, idMap, args[0], false );
		final I_ItemData page = source.getStore().items().getItem( source.getID(), false );

		final ArrayTable<String> tab = new ArrayTable<>( 2 );

		if( this.filter == null )
			tab.addRow( "ID", page.getID().getAbsolute() );
//		sb.append("  ID:      ");
//		sb.append(page.getID().generate());
//		sb.append('\n');

		if( this.filter == null || this.filter == META_TYPE.TITLE )
			tab.addRow( "Title", page.getTitle() );
		if( this.filter == null || this.filter == META_TYPE.HITS )
			tab.addRow( "Hits", "" + page.getHits() );
		if( this.filter == null || this.filter == META_TYPE.CREATED )
			tab.addRow( "Created", page.getCreated().toString() );
		if( this.filter == null || this.filter == META_TYPE.CHANGED )
			tab.addRow( "Changed", page.getChanged().toString() );
		if( this.filter == null || this.filter == META_TYPE.TAGS )
			tab.addRow( "Tags", ConvertArray.toString( ", ", page.getTags() ) );
		if( this.filter == null || this.filter == META_TYPE.TRASH )
			tab.addRow( "Trash", "" + page.getTrash() );
		// crypt
		tab.addRow( "Size", page.getSizeString() );

		final StringBuilder sb = new StringBuilder();

		for( final String[] row : tab ) {
			if( sb.length() > 0 )
				sb.append( '\n' );
			sb.append( TAG_TERMINAL.INDENT_STRING );
			sb.append( row[0] );
			sb.append( ":" );
			sb.append( Lib_String.sequence( ' ', 8 - row[0].length() ) );
			sb.append( row[1] );
		}
		Lib_TTOut.printRaw( sb.toString() );
//		return source.getID();
//		return new IdMap(source.getID());
		return new IdMap( idMap, args[0] );
	}

}
