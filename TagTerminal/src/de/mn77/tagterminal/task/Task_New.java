/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Collection;
import java.util.HashSet;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MInput;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.tagterminal.util.Lib_Task;


/**
 * @author Michael Nitsche
 * @created 15.02.2022
 */
public class Task_New extends A_Task_Store {

	private final boolean gui;


	public Task_New( final I_DataStoreManager dsm, final String[] input, final boolean gui ) {
		super( dsm, input );
		this.gui = gui;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final String[] args, final Integer storeID ) {
		final I_Store store = storeID == null
			? dsm.getWriteStore()
			: dsm.getDataStoreMap().get( storeID );

		Lib_Task.checkWrite( store );

		try {
			final I_ItemData sNew = dsm.tool().newItem( this.gui );

			final ItemID newID = store.items().add( sNew );

			if( newID == null ) {
				Lib_TTOut.print( "The new page could not be created!" );
				return null;
			}
			Lib_TTOut.print( "Which tags to use for the new page?" );
			Lib_TTOut.echoIndent();
			final String s = MInput.readStringDefault( "" );
			final HashSet<String> tags = ConvertString.toItemSet( new char[]{ ' ', ',' }, s );
			final SimpleList<String> usedTags = new SimpleList<>(tags.size());

			if( tags.size() > 0 ) {
				final ItemAccess source = new ItemAccess( dsm, newID, newID.getAbsolute(), true );

				for( String tag : tags ) {
					tag = Lib_Comply.normTag( tag );
					source.getStore().tags().classify( source.getID(), tag );
					usedTags.add( tag );

					final Collection<String> subTags = dsm.getTagLinks( tag );
					for( final String sub : subTags ) {
						source.getStore().tags().classify( source.getID(), sub );
						usedTags.add( sub );
					}
				}
			}
			Lib_TTOut.print( "New page saved with ID: " + newID.getAbsolute() );
			return new IdMap( newID, sNew.getTitle(), ConvertSequence.toArray( String.class, usedTags ) );
		}
		catch( final TagTerminalException e ) {
			throw e;
		}
		catch( final Exception e ) {
			throw Err.exit( e );
		}
	}

}
