/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.store.I_Store;


/**
 * @author Michael Nitsche
 * @created 24.10.2024
 */
public class Task_OnOff extends A_Task_Store {

	private final boolean enable;


	public Task_OnOff( final I_DataStoreManager dsm, final String[] input, boolean enable ) {
		super( dsm, input );
		this.enable = enable;
	}


	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final String[] args, final Integer storeID ) {
		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMapFull().entrySet() ) {
			if(storeID == null || store.getKey() == storeID) {
				store.getValue().setEnabled(this.enable);
				break;
			}
		}

		return new IdMap();
	}

}
