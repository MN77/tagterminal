/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.base.data.util.U_StringArray;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Task_SearchTags extends A_Task {

	private final boolean or;


	public Task_SearchTags( final I_DataStoreManager dsm, final IdMap idMap, final String[] input, final boolean or ) {
		super( dsm, idMap, input, 1, null );
		this.or = or;
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		Integer storeID = null;

		String[] search = args;

		if( search.length >= 1 && search[0].matches( "[1-9]" ) ) {
			storeID = Lib_Comply.checkStoreID( dsm, search[0] );
			search = U_StringArray.cutFrom( search, 1 );
		}
		if( search.length == 0 )
			throw new TagTerminalException( "No tags given to search for!" );

		for( int i = 0; i < search.length; i++ )
			search[i] = search[i].toLowerCase(); // Invalid tags are allowed here!

		final IdMap newMap = new IdMap();

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( !store.getValue().canTags() || storeID != null && store.getKey() != storeID )
				continue;

			final SearchResult result = store.getValue().items().searchTags( search, this.or );
			newMap.put( store.getKey(), result );
		}
		Lib_TTOut.showSearchResult( newMap );
		return newMap;
	}

}
