/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import java.util.Map.Entry;

import de.mn77.base.data.struct.CountingSet;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.SimpleSet;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Task_Tree extends A_Task {

	private static final int    AMOUNT_INDENT = 25;
	private static final char   AMOUNT_OPEN   = '(';
	private static final char   AMOUNT_CLOSE  = ')';
	private static final String NODE_INDENT   = "   ";
	private static final String NODE_PREFIX   = " • "; // → • \\  └─
	private static final String STORE_OPEN    = "\nStore <";
	private static final String STORE_CLOSE   = ">";


	public Task_Tree( final I_DataStoreManager dsm, final IdMap map, final String[] args ) {
		super( dsm, map, args, 0, 1 );
	}


	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final Integer storeID = this.iArgumentStore( dsm, args );

		for( final Entry<Integer, I_Store> store : dsm.getDataStoreMap().entrySet() ) {
			if( storeID != null && store.getKey() != storeID )
				continue;

			this.iComputeStore( store );
		}

		return idMap;
	}


	private Integer iArgumentStore( final I_DataStoreManager dsm, final String[] args ) {
		return args.length == 0
			? null
			: Lib_Comply.checkStoreID( dsm, args[0] );
	}


	private void iComputeStore( final Entry<Integer, I_Store> store ) {
		final SearchResult result = store.getValue().items().searchAll();

		final CountingSet<String> counter = new CountingSet<>();
		final SimpleList<String[]> allTags = new SimpleList<>();

		for( final SearchResultItem item : result ) {
			final String[] itemTags = store.getValue().tags().getClasses( item.getID() );
			allTags.add( itemTags );

			for( final String tag : itemTags )
				counter.inc( tag );
		}

		counter.sort( true );
		final String[] sortedTags = counter.toKeyArray( new String[counter.size()] );

		for( int i = 0; i < allTags.size(); i++ ) {
			final String[] unsorted = allTags.get( i );
			if( unsorted.length > 0 )
				allTags.set( i, this.iSortTags( sortedTags, unsorted ) );
		}

		this.iOutputNode( store.getKey(), allTags, new String[0] );
	}

	private void iIndentAmount( final StringBuilder sb, final int amount_only, final int amount_all ) {
		while( sb.length() < Task_Tree.AMOUNT_INDENT - 2 )
			sb.append( ' ' );

		sb.append( "  " );
		sb.append( Task_Tree.AMOUNT_OPEN );
		sb.append( amount_only );

		if( amount_only != amount_all ) {
			sb.append( "/" );
			sb.append( amount_all );
		}
		sb.append( Task_Tree.AMOUNT_CLOSE );
	}

	private void iOutputNode( final Integer storeKey, final SimpleList<String[]> allTags, final String[] path ) {
		final StringBuilder sb = new StringBuilder();
		final int depth = path.length;

		if( depth == 0 ) {
			sb.append( Task_Tree.STORE_OPEN );
			sb.append( storeKey );
			sb.append( Task_Tree.STORE_CLOSE );

			int amount_only = 0;
			int amount_all = 0;
			final SimpleSet<String> rootTags = new SimpleSet<>();

			for( final String[] tags : allTags ) {
				amount_all++;

				if( tags.length == 0 )
					amount_only++;
				else
					rootTags.add( tags[0] );
			}

			this.iIndentAmount( sb, amount_only, amount_all );
			Lib_TTOut.printRaw( sb.toString() );

			rootTags.sort();
			for( final String tag : rootTags )
				this.iOutputNode( storeKey, allTags, new String[]{ tag } );
		}
		else {
			for( int i = 0; i < depth - 1; i++ )
				sb.append( Task_Tree.NODE_INDENT );
			sb.append( Task_Tree.NODE_PREFIX );

			sb.append( path[path.length - 1] );

			int amount_only = 0;
			int amount_all = 0;
			final SimpleSet<String> nodeTags = new SimpleSet<>();

			for( final String[] tags : allTags )
				if( this.iStartsWith( tags, path ) ) {
					amount_all++;
					if( tags.length == depth )
						amount_only++;
					if( tags.length > depth )
						nodeTags.add( tags[depth] );
				}

			if( amount_all == 0 )
				return;

			this.iIndentAmount( sb, amount_only, amount_all );
			Lib_TTOut.printRaw( sb.toString() );

			nodeTags.sort();

			for( final String tag : nodeTags ) {
				final String[] newPath = new String[path.length + 1];
				System.arraycopy( path, 0, newPath, 0, path.length );
				newPath[depth] = tag;

				this.iOutputNode( storeKey, allTags, newPath );
			}
		}
	}

	private String[] iSortTags( final String[] sortedTags, final String[] unsorted ) {
		if( unsorted.length == 0 )
			return unsorted;

		final String[] result = new String[unsorted.length];
		int pointer = 0;

		for( final String curTag : sortedTags )
			for( final String tag : unsorted )
				if( tag.equals( curTag ) ) {
					result[pointer] = curTag;
					pointer++;

					if( pointer == unsorted.length )
						return result;

					break;
				}

		return result;
	}

	private boolean iStartsWith( final String[] arr, final String[] search ) {
		if( arr.length < search.length )
			return false;

		for( int i = 0; i < search.length; i++ )
			if( !arr[i].equals( search[i] ) )
				return false;

		return true;
	}

}
