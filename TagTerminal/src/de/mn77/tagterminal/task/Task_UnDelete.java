/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemAccess;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 21.02.2022
 */
public class Task_UnDelete extends A_Task_Item {

	public Task_UnDelete( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 1, null, false );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		String lastMId = null;

		for( final String id : args ) {
			final ItemAccess source = new ItemAccess( dsm, idMap, id, true );

			if( args.length == 1 )
//				result = source.getID();
				lastMId = id;

			final boolean isInTrash = source.getStore().items().isInTrash( source.getID() );

			if( isInTrash ) {
				source.getStore().items().unTrash( source.getID() );
				Lib_TTOut.print( "Undeleted: " + source.getID().getAbsolute() );
			}
			else
				Lib_TTOut.print( "Page is not in the trash: " + source.getID().getAbsolute() );
		}
		return new IdMap( idMap, lastMId );
	}

}
