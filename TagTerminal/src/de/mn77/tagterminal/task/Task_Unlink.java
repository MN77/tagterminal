/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.task;

import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.util.Lib_Comply;
import de.mn77.tagterminal.util.Lib_TTOut;


/**
 * @author Michael Nitsche
 * @created 16.09.2022
 */
public class Task_Unlink extends A_Task {

	public Task_Unlink( final I_DataStoreManager dsm, final IdMap map, final String[] input ) {
		super( dsm, map, input, 2, 2 );
	}

	@Override
	protected IdMap exec( final I_DataStoreManager dsm, final IdMap idMap, final String[] args ) {
		final String tagBase = args[0];
		final String tagTarget = args[1];

		Lib_Comply.checkTag( tagBase );
		Lib_Comply.checkTag( tagTarget );

		final boolean done = dsm.unlinkTags( tagBase, tagTarget );

		final String message = done
			? "Link removed: " + tagBase + " --> " + tagTarget
			: "Not linked: " + tagBase + " --> " + tagTarget;
		Lib_TTOut.print( message );

		return idMap;
	}

}
