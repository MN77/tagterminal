/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import de.mn77.base.error.Err;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.exception.TagTerminalException;


/**
 * @author Michael Nitsche
 * @created 25.02.2022
 */
public class Lib_Comply {

	/**
	 * @return Returns true, if the given id is a valid absolute id. Returns false if the given id is a mapped id.
	 * @throws Throws a Exception, when the given String is not a ID.
	 */
	public static boolean checkAbsoluteID( final String id ) {
		Err.ifNull( id );
		Lib_Comply.checkItemID( id );
		return id.charAt( 1 ) == Lib_ItemID.ABSOLUTE_ID_DELIMITER;
	}

	/**
	 * @apiNote Check the given id for valid mapped or absolute id.
	 */
	public static void checkItemID( final String s ) {
		final int len = s.length();

		if(len < 2)
			Lib_Comply.throwInvalidItemID( s );

		final int offset = s.charAt( 1 ) == Lib_ItemID.ABSOLUTE_ID_DELIMITER
			? 2
			: 1;
		if( len < offset + 1 )
			Lib_Comply.throwInvalidItemID( s );

		for( int i = offset; i<len; i++) {
			final char c = s.charAt( i );

			if(c < '0' || c > '9')
				Lib_Comply.throwInvalidItemID( s );
		}
	}

	public static Integer checkStoreID( final I_DataStoreManager dsm, final String sid ) {
		if( sid == null )
			return null;

		if( sid.length() == 1 && sid.charAt( 0 ) >= '1' && sid.charAt( 0 ) <= '9' ) {
			final int storeID = Integer.parseInt( sid );

			if( !dsm.getDataStoreMapFull().containsKey( storeID ) )
				throw new TagTerminalException( "This store ID is not defined: " + storeID );

			return storeID;
		}

		throw new TagTerminalException( "This is not a valid store ID: " + sid );
	}

	public static void checkTag( final String tag ) {
		if( tag.length() < 3 )
			throw new TagTerminalException( "A tag must be at least 3 chars long: " + tag );
		if( tag.length() > 16 )
			throw new TagTerminalException( "The maximum length of a tag is 16: " + tag );

		final char c0 = tag.charAt( 0 );
		if( !(c0 >= 'a' && c0 <= 'z' || c0 >= 'à' && c0 <= 'ÿ' && c0 != '÷') )
			throw new TagTerminalException( "A tag must begin with a lower case char: " + tag );

		for( final char c : tag.toCharArray() )
			if( !(c >= 'a' && c <= 'z' || c >= 'à' && c <= 'ÿ' && c != '÷' || c >= '0' && c <= '9' || c == '_') )
				throw new TagTerminalException( "Invalid char in tag! Allowed are only 'a-z', '0-9' and '_': " + tag );
	}

	/**
	 * @apiNote Tag will have 3-16 lowercase chars. Invalid chars will be replaced by '_'
	 */
	public static String normTag( final String tag ) {
		final StringBuilder sb = new StringBuilder( tag.toLowerCase() );

		while( sb.length() < 3 )
			sb.append( '_' );

		if( sb.length() > 16 )
			sb.setLength( 16 );

		final char c0 = sb.charAt( 0 );
		if( !(c0 >= 'a' && c0 <= 'z' || c0 >= 'à' && c0 <= 'ÿ' && c0 != '÷') )
			sb.setCharAt( 0, 'a' );

		for( int i = 0; i < sb.length(); i++ ) {
			final char c = sb.charAt( i );
			if( !(c >= 'a' && c <= 'z' || c >= 'à' && c <= 'ÿ' && c != '÷' || c >= '0' && c <= '9' || c == '_') )
				sb.setCharAt( i, '_' );
		}

		final String result = sb.toString();
		if( !tag.equals( result ) )
			Lib_TTOut.print( "Tag '" + tag + "' modified to: '" + result + "'" );

		return result;
	}

	private static void throwInvalidItemID(String s) {
		throw new TagTerminalException( "This is not a valid item ID: " + s );
	}

}
