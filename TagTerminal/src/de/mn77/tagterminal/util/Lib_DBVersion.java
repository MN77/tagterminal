/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 08.04.2023
 * @apiNote Encode major and minor to one single number.
 */
public class Lib_DBVersion {

	public static int[] decode( final int version ) {
		final int major = version >> 10;
		final int minor = version - (major << 10);
		return new int[]{ major, minor };
	}

	public static int encode( final int major, final int minor ) {
		Err.ifOutOfBounds( 1, 999, major, minor );
		return (major << 10) + minor;
	}


	public static void main( final String[] args ) {
		MOut.print( Lib_DBVersion.encode( 1, 1 ) );
		MOut.print( Lib_DBVersion.decode( 1025 ) );

		MOut.print( Lib_DBVersion.encode( 5, 3 ) );
		MOut.print( Lib_DBVersion.decode( 5123 ) );
	}

}
