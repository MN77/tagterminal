/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.tagterminal.data.I_DataStoreManager;


/**
 * @author Michael Nitsche
 * @created 28.02.2022
 */
public class Lib_External {

	// Examples: first = "default", second = default editor
	public static final String[] XEDITORS = { "default", "xedit", "mousepad", "xfwrite", "gedit", "pluma", "kate" }; // examples
	public static final String[] EDITORS  = { "default", "editor", "mcedit", "nano" }; // examples


	public static void editFile( final I_DataStoreManager dsm, final String absolutePath, final boolean gui ) throws Exception {
//		SysCmd.start("editor", tmpFile.getAbsolutePath(), true);
//		SysCmd.start("export LANG='de_DE.UTF-8'; editor", tmpFile.getAbsolutePath(), true);
//		MOut.print("File-Encoding: "+Sys.getFileEncoding());
//		SysCmd.start("locale -a; editor", absolutePath, true);

		// PROBLEM! Keyset for editor is ASCII	// same for mcedit, nano

//		SysCmd.start("export LANG='de_DE.UTF-8'; export LC_CTYPE=de_DE; editor", absolutePath, true);
//		SysCmd.start("export LANG='de_DE'; export LC_CTYPE=de_DE; editor", absolutePath, true);

		final String conf = gui
			? dsm.getEditEditorX()
			: dsm.getEditEditor();

		final boolean useDefault = conf == null || conf.length() == 0 || conf.toLowerCase().equals( "default" );

		final String cmd = useDefault
			? gui
				? Lib_External.XEDITORS[1]
				: Lib_External.EDITORS[1]
			: conf;

		Lib_External.iStart( cmd, absolutePath );

//		SysCmd.start("LANG=de_DE.UTF-8 editor", absolutePath, true);
//		LC_CTYPE=de_DE
	}

	private static void iStart( final String command, final String absolutePath ) throws Exception {
		SysCmd.exec( true, true, command, absolutePath );
	}

}
