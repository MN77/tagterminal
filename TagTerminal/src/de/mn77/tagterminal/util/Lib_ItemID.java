/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 09.05.2021
 */
public class Lib_ItemID {

	//  Doublepoint is not a good choice: 1:234
	public static final char ABSOLUTE_ID_DELIMITER = '#';	//    1#234


	/**
	 * @apiNote Combines only 'Store-ID' with delimiter and 'absolute Page-ID'
	 */
	public static String encodeAbsolute( final int dataStore, final int id ) {
		Err.ifOutOfBounds( 1, 9, dataStore );
		Err.ifTooSmall( 0, id );
		final StringBuilder sb = new StringBuilder();
		sb.append( dataStore );
		sb.append( Lib_ItemID.ABSOLUTE_ID_DELIMITER );
		sb.append( id );
		return sb.toString();
	}

	/**
	 * @return Returns true, if the given string is a mapped or absolute ID. (105, 1#23)
	 */
	public static boolean isItemID(String s) {
//		args[i].matches( "[0-9#]+" ) )

		if(s == null)
			return false;

		final int len = s.length();

		if(len == 0)
			return false;

		final char c0 = s.charAt( 0 );
		if(c0 < '0' || c0 > '9')
			return false;

		// Okay, it is not a tag. Now check if this is a valid absolute or mapped id.
		Lib_Comply.checkItemID( s );
		return true;
	}

}
