/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.container.IdMap;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultEntry;


/**
 * @author Michael Nitsche
 * @created 25.02.2022
 */
public class Lib_TTOut {

	public static final String KEY_TITLE_DELIMITER = " | ";
	private static final int TITLE_LEN_MAX = 60;


	public static void checkLines( final List<String> lines, String lineStyles ) {

		if( lines.size() != lineStyles.length() ) {
			if( TAG_TERMINAL.DEBUG )
				throw new Err_Runtime( "Amount of lines is not equal with the amount of styles: " + lines.size() + " <--> " + lineStyles.length() );

			lineStyles = FormString.length( lines.size(), lineStyles, true );
		}
	}

	public static void echoIndent() {
		MOut.echo( TAG_TERMINAL.INDENT_STRING );
	}

	public static void print() {
		MOut.print();
	}

	public static void print( final Object o ) {
//		MOut.print(TAG_TERMINAL.INDENT_STRING + o);

		final List<String> lines = ConvertString.toLines( "" + o );

		final StringBuilder sb = new StringBuilder();

		for( final String line : lines ) {
			sb.append( TAG_TERMINAL.INDENT_STRING );
			sb.append( line );
			sb.append( '\n' );
		}

		MOut.echo( sb.toString() );
	}

	public static void printRaw( final Object o ) {
		MOut.print( o );
	}

	public static void showMapping( final IdMap idMap ) {
		final ArrayTable<Object> tab = new ArrayTable<>( 4, idMap.size() + 1 );
		tab.addRow( "Key", "Store", "ID", "Title" );

		for( final SearchResultEntry s : idMap )
			tab.addRow( s.key, s.item.getID().storeID, s.item.getID().internalID, s.item.getTitle() );


		final TableStyler styler = new TableStyler();
		styler.colLineStyleDefault( '|' );
//		styler.setUnicode(true);
		styler.alignColumn( 0, ALIGN.LEFT );
		styler.alignColumn( 1, ALIGN.CENTER );
		styler.alignColumn( 2, ALIGN.RIGHT );
		styler.rowLineStyles( "=" );	// +=' '
		styler.setIndent( TAG_TERMINAL.INDENT_NUM );
//		styler.setBorder(true);

		final String s = styler.compute( tab );
		MOut.print( s );
	}

	public static IdMap showResult( final I_Iterable<ItemMetaData> data, final int maxAmount, final ITEM_META_TYPE type ) {
		// amount is automaticaly limited to 0
		final HashMap<Integer, SearchResult> potmap = new HashMap<>();

		for( int line = 0; line < data.size() && line < maxAmount; line++ ) {
			final ItemMetaData s = data.get( line );
			final int storeID = s.getID().storeID;

			if( potmap.containsKey( storeID ) )
				potmap.get( storeID ).add( s );
			else
				potmap.put( storeID, new SearchResult(s) );
		}
		final int c2max = 10;
//		int c2max = 0;
//		switch(type) {
//			case HITS:
//				c2max = Integer.toString(maxID).length();
//				break;
//			case CREATED:
//			case CHANGED:
//				c2max = 10;
//				break;
//		}

		final IdMap map = new IdMap();
		for( final Entry<Integer, SearchResult> e : potmap.entrySet() )
			map.put( e.getKey(), e.getValue() );

		Lib_TTOut.iOutputMeta( map, c2max, type );
		return map;
	}

	public static void showSearchResult( final IdMap data ) {
		int max = 0;

		for( final SearchResultEntry s : data )
			if(s.item.getTitle().length() > max)
				max = s.item.getTitle().length();

		if( max > Lib_TTOut.TITLE_LEN_MAX )
			max = Lib_TTOut.TITLE_LEN_MAX;

		for( final SearchResultEntry s : data ) {
			final StringBuilder sb = new StringBuilder();
			sb.append( TAG_TERMINAL.INDENT_STRING );
			sb.append( s.key );
			sb.append( Lib_TTOut.KEY_TITLE_DELIMITER );

			final String title = s.item.getTitle();
			if(title.length() > Lib_TTOut.TITLE_LEN_MAX-1) {
				sb.append( title.substring( 0, Lib_TTOut.TITLE_LEN_MAX-1 ) );
				sb.append( '…' );
			}
			else
				sb.append( title );

			if( data.hasTags() ) {
				for(int len = title.length(); len < max; len++)
					sb.append( ' ' );

				sb.append( Lib_TTOut.KEY_TITLE_DELIMITER );

				if(s.item.getTags() != null) {
					boolean firstTag = true;

					for(String tag : s.item.getTags() ) {
						if(!firstTag)
							sb.append( ", " );
						firstTag = false;

						sb.append( tag );
					}
				}
			}

			MOut.print( sb.toString() );
//			MOut.print( TAG_TERMINAL.INDENT_STRING + s.key + Lib_TTOut.KEY_TITLE_DELIMITER + s.item.getTitle() );
		}
	}

	private static void iOutputMeta( final IdMap data, final int col2max, final ITEM_META_TYPE type ) {

		for( final SearchResultEntry g : data ) {
			final ItemMetaData meta = (ItemMetaData)g.item;
			final StringBuilder sb = new StringBuilder();
			sb.append( TAG_TERMINAL.INDENT_STRING );
			sb.append( g.key );
			sb.append( " | " );

			switch( type ) {
				case HITS:
					sb.append( FormNumber.right( col2max, meta.getHits() ) );
					break;
				case SIZE:
					sb.append( FormNumber.right( col2max, meta.getSize() ) );
					break;
				case CREATED:
					sb.append( meta.getCreated().getDate().toString() );
					break;
				case CHANGED:
					sb.append( meta.getChanged().getDate().toString() );
					break;
			}
			sb.append( " | " );
			sb.append( meta.getTitle() );

			MOut.print( sb.toString() );
		}
	}

}
