/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store;


/**
 * @author Michael Nitsche
 * @created 23.02.2022
 */
public class Lib_Task {

	public static void checkTrash( final I_Store store ) {
		if( !store.canTrash() )
			throw new TagTerminalException( "This store does not support trash functionality: " + store.getStoreID() );
	}

	public static void checkWrite( final I_Store store ) {
		if( store == null )
			throw new TagTerminalException( "Missing or invalid store definition!" );
		if( !store.canWrite() )
			throw new TagTerminalException( "This store is readonly: " + store.getStoreID() );
	}

	/**
	 * @return Returns true if there are no arguments.
	 */
	public static boolean emptyInput( final String[] args ) {
		return args == null || args.length == 0 || args.length == 1 && args[0].trim().length() == 0;
	}

	public static ARG_TYPE identify( final String arg ) {
		final int len = arg.length();

		if( len >= 1 ) {
			final char c0 = arg.charAt( 0 );
			final boolean isNumber = c0 >= '1' && c0 <= '9';

			if( len == 1 && isNumber )
				return ARG_TYPE.STORE;
			if( isNumber )
				return ARG_TYPE.ITEM;
			if( !isNumber )
				return ARG_TYPE.TAG;
		}
		throw new TagTerminalException( "Got unknown argument: " + arg );
	}

}
