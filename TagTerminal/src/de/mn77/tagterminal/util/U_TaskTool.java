/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the TagTerminal library <https://gitlab.com/MN77/tagterminal>
 *
 * TagTerminal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TagTerminal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TagTerminal. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.tagterminal.util;

import java.io.File;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.MDir;
import de.mn77.tagterminal.data.EDIT_RESULT;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.exception.TagTerminalException;


/**
 * @author Michael Nitsche
 * @created 07.11.2024
 */
public class U_TaskTool {

	public static void export_print( final String id, final File file ) {
		Lib_TTOut.print( "Exported " + id + " to:" );
		Lib_TTOut.print( file.getAbsolutePath() );
	}

	public static void exportToTxtFile( final MDir dir, final SearchResultItem item, final String content ) {
		final String id = item.getID().getAbsolute();

		final StringBuilder filename = new StringBuilder();
		filename.append( id );
		filename.append( " - " );
		filename.append( item.getTitle() );
		filename.append( ".txt" );

		try {
			final File file = dir.fileAbsent( filename.toString() ).getFile();
			Lib_TextFile.set( file, content );
			U_TaskTool.export_print( id, file );
		}
		catch( final Err_FileSys e ) {
			throw new TagTerminalException( e.getMessage() );
		}
	}

	public static Group2<EDIT_RESULT, I_ItemData> rename( final I_ItemData s, final boolean gui, final String prefix, final I_DataStoreManager dsm ) {
		final String title = s.getTitle();

		try {
			final File tmpFile = File.createTempFile( prefix, ".txt" );
			Lib_TextFile.set( tmpFile, title );
			Lib_External.editFile( dsm, tmpFile.getAbsolutePath(), gui );
			String result = Lib_TextFile.read( tmpFile );
			tmpFile.delete();

			if( title.equals( result ) )
				return new Group2<>( EDIT_RESULT.SAME, s );
			else {
				result = result.trim();
				s.setTitle( result );
				final boolean hasContent = result.length() > 0;
				final EDIT_RESULT state = hasContent ? EDIT_RESULT.CHANGED : EDIT_RESULT.DELETED;
				return new Group2<>( state, s );
			}
		}
		catch( final TagTerminalException e ) {
			throw e;
		}
		catch( final Exception e ) {
//			MOut.error(e);
			throw new TagTerminalException( e.getMessage() );
		}
	}


	private U_TaskTool() {}

}
